<?php

namespace App\Mail;

use App\Bulletin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class BulletinMailApproval extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bulletin $bulletin)
    {
        $this->bulletin = $bulletin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dt = new Carbon($this->bulletin->date_post);
        $dt_string = $dt->toDateString();
        return $this->markdown('emails.bulletin.mail-approval')
            ->subject('Intranet - Itu Central - Aprovação de Comunicado: ' . $this->bulletin->subject)
            ->with([
                'bulletinMessage' => $this->bulletin->message,
                'bulletinId' => $this->bulletin->id,
                'bulletinDate' => $dt_string,
            ]);
    }
}
