<?php

namespace App\Mail;

use App\Bulletin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class BulletinMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $bulletin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bulletin $bulletin)
    {
        $this->bulletin = $bulletin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dt = new Carbon($this->bulletin->date_post);
        $dt_string = $dt->format('d-m-Y');
        switch ($this->bulletin->status) {
            case 0:
                return $this->markdown('emails.bulletin.mail-create')
                    ->subject('Intranet Itu Central - Comunicado Solicitado: ' . $this->bulletin->subject)
                    ->with([
                        'bulletinMessage' => $this->bulletin->message,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string,
                    ]);
                break;
            case 3:
                return $this->markdown('emails.bulletin.mail-reject')
                    ->subject('Intranet Itu Central - Comunicado Rejeitado: ' . $this->bulletin->subject)
                    ->with([
                        'bulletinMessage' => $this->bulletin->message,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string,
                    ]);
                break;
            case 4:
                return $this->markdown('emails.bulletin.mail-accept')
                    ->subject('Intranet Itu Central - Comunicado Aprovado: ' . $this->bulletin->subject)
                    ->with([
                        'bulletinMessage' => $this->bulletin->message,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string,
                    ]);
                break;
            case 5:
                return $this->markdown('emails.bulletin.mail')
                    ->subject('Intranet Itu Central - Novo Comunicado: ' . $this->bulletin->subject)
                    ->with([
                        'bulletinMessage' => $this->bulletin->message,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string,
                    ]);
                break;
        }
    }
}
