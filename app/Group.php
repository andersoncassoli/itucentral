<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Group extends Model
{
    use Sortable;

    public $sortable = ['id', 'name'];

    protected $fillable = [
        'name'
    ];
    //protected $guarded = ['admin']
    
    public function bulletins()
    {
        return $this->belongsToMany('App\Models\Bulletin');
    }
    

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_groups');
    }
}
