<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use Sortable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $sortable = ['id', 'name', 'email', 'description', 'created_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'users_groups');
    }

    public function bulletins()
    {
        return $this->belongsToMany(Bulletin::class, 'bulletins_users');
    }

    public function my_bulletins()
    {
        return $this->belongsToMany(Bulletin::class, 'my_bulletins', 'user_id', 'bulletin_id')->withPivot('read_at');
    }

    public function bulletins_owner()
    {
        return $this->hasMany('App\Models\Bulletin');
    }

    public function bulletins_app()
    {
        return $this->belongsToMany(Bulletin::class, 'bulletin_approval', 'user_id', 'bulletin_id')->withPivot('acceptyn', 'comment');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /*
     *
     * Permissons session
     * 
    */

    /*
    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }

    public function hasAnyRoles($roles)
    {
        if( is_array($roles) || is_object($roles) ) {
            dd($this->roles);
            foreach( $roles as $role ) {
                return $this->roles->contains('name', $role->name);
            }
        }
       
        return $this->roles->contains('name', $roles);
    }
    */

    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
    }

    public function hasAnyRoles($roles)
    {
        if (is_array($roles) || is_object($roles)) {
            return !!$roles->intersect($this->roles)->count();
        }

        return $this->roles->contains('name', $roles);
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }
}
