<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Bulletin extends Model
{
    use Sortable;

    protected $fillable = [
        'subject', 'priority', 'area', 'message', 'date_post', 'date_email_sent', 'status', 'file_name', 'file_url', 'file_original_name', 'area', 'copied_from'
    ];

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'bulletins_groups')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'bulletins_users')->withTimestamps();
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function my_users()
    {
        return $this->belongsToMany(User::class, 'my_bulletins', 'bulletin_id', 'user_id')->withPivot('read_at')->withTimestamps();
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function approvals()
    {
        return $this->belongsToMany(User::class, 'bulletin_approval', 'bulletin_id', 'user_id')->withPivot('acceptyn', 'comment', 'updated_at')->withTimestamps();
    }

    public function copied()
    {
        return $this->belongsTo('App\Bulletin', 'copied_from', 'id');
    }
}
