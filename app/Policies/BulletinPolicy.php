<?php

namespace App\Policies;

use App\User;
use App\Bulletin;
use Illuminate\Auth\Access\HandlesAuthorization;

class BulletinPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewBulletin(User $user, Bulletin $bulletin)
    {
        //return $user->id == $bulletin->created_by;
    }
}
