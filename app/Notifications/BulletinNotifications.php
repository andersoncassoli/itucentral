<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use App\Bulletin;

class BulletinNotifications extends Notification implements ShouldQueue
{
    use Queueable;

    private $bulletin;
    private $key;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Bulletin $bulletin, $key)
    {
        $this->bulletin = $bulletin;
        $this->key = $key;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //$url = config('app.url').'/bulletin/'.$this->bulletin->id;
        $dt = new Carbon($this->bulletin->date_post);
        $dt_string = $dt->format('d-m-Y');
        $url = route('bulletin.show', $this->bulletin->id);
        switch ($this->key) {
            case 'create':
                return (new MailMessage)
                    ->subject("Intranet Itu Central - Comunicado Solicitado: " . $this->bulletin->subject)
                    ->markdown('emails.bulletin.mail-create', [
                        'url' => $url,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string
                    ]);
                break;
            case 'approval':
                return (new MailMessage)
                    ->subject("Intranet Itu Central - Aprovação de Comunicado:  " . $this->bulletin->subject)
                    ->markdown('emails.bulletin.mail-approval', [
                        'url' => $url,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string
                    ]);
                break;
            case 'reject':
                return (new MailMessage)
                    ->subject("Intranet Itu Central - Comunicado Rejeitado: " . $this->bulletin->subject)
                    ->markdown('emails.bulletin.mail-reject', [
                        'url' => $url,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string
                    ]);
                break;
            case 'accept':
                return (new MailMessage)
                    ->subject("Intranet Itu Central - Comunicado Aprovado: " . $this->bulletin->subject)
                    ->markdown('emails.bulletin.mail-accept', [
                        'url' => $url,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string
                    ]);
                break;
            case 'publish':
                return (new MailMessage)
                    ->subject("Intranet Itu Central - Novo Comunicado: " . $this->bulletin->subject)
                    ->markdown('emails.bulletin.mail-publish', [
                        'url' => $url,
                        'bulletinId' => $this->bulletin->id,
                        'bulletinDate' => $dt_string
                    ]);
                break;
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
