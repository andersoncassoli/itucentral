<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function bulletin()
    {
        return $this->belongsTo(Bulletin::class);
    }
}
