<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\User;
use App\Group;
use App\Role;

class UserController extends Controller
{

    public function __construct(User $user, Group $group, Role $role)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->group = $group;
        $this->role = $role;
    }

    public function index()
    {
        $user_auth = auth()->user()->hasRole("Administração");
        if (!$user_auth) {
            return view('errors.404');
        }
        $users = $this->user->sortable(['name' => 'asc'])->paginate(100);
        return view('users', compact('users'));
    }

    public function contact($character)
    {
        if ($character == '*') {
            $users = $this->user->sortable(['name' => 'asc'])->paginate(21);
        } else {
            $users = $this->user->where('name', 'LIKE', $character . '%')->sortable(['name' => 'asc'])->paginate(21);
        }
        return view('contacts', compact('users'));
    }

    public function create()
    {
        return view('create-user');
    }

    public function store(Request $request)
    {
        $user = $this->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make(str_random(8));
        //dd($user);
        $store = $user->save();

        return redirect()->route('user.create');
    }

    public function edit($id)
    {
        $user = $this->user->find($id);
        $selected_groups = $user->groups;
        $grps = $this->group->all();
        foreach ($grps as $grp) {
            $groups[$grp->id] = $grp->name;
        }
        $selected_roles = $user->roles;
        $rls = $this->role->all();
        foreach ($rls as $rl) {
            $roles[$rl->id] = $rl->name;
        }

        return view('edit-user', compact('user', 'groups', 'roles', 'selected_groups', 'selected_roles'));
    }

    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $update_groups = $user->groups()->sync($request->groups);
        $update_roles = $user->roles()->sync($request->roles);

        if ($update_groups and $update_roles)
            return redirect()->route('user.index');
        else
            return redirect()->route('user.edit', $id)->with(['errors' => 'Falha ao editar']);
    }

    public function destroy($id)
    {
        $user = $this->user->find($id);
        $user->delete();
        return redirect()->route('user.index');
    }

    public function change_pass(Request $request)
    {
        $user = $this->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make(str_random(8));
        //dd($user);
        $store = $user->save();

        return redirect()->route('user.create');
    }

    public function change_approvation($id)
    {
        $user = $this->user->find($id);
        if ($user->approval == 0) {
            $user->approval = 1;
        } else {
            $user->approval = 0;
        }
        $user->update();
        return redirect()->route('user.index');
    }

    public function update_info()
    {
        $users = $this->user->whereNull('ou_name')->get();
        foreach ($users as $user) {
            if (isset($user->add_info)) {
                $infos = explode(',', $user->add_info);
                $user_info = explode('-', substr($infos[1], 3));
                $user->ou_name = trim($user_info[0]);
                if (isset($user_info[1])) {
                    $user->city = trim($user_info[1]);
                }
                $user->save();
                Log::info('Updated user:' . $user->id . ' - ' . $user->name . 'with addictional information...');
            }
        }
    }

    /*
     * Marcado para remoção
     * 
    public function getad()
    {
        // Construct new Adldap instance.
        $ad = new \Adldap\Adldap();

        // Create a configuration array.
        $config = [  
        // An array of your LDAP hosts. You can use either
        // the host name or the IP address of your host.
        'hosts'    => ['172.21.105.13'],

        // The base distinguished name of your domain to perform searches upon.
        'base_dn'  => 'dc=gifcorporatico,dc=itu',

        // The account to use for querying / modifying LDAP records. This
        // does not need to be an admin account. This can also
        // be a full distinguished name of the user account.
        'username' => 'intranet@gifcorporativo.itu',
        'password' => 'mobitap@123',
        ];

        // Add a connection provider to Adldap.
        $ad->addProvider($config);

        try {
            // If a successful connection is made to your server, the provider will be returned.
            $provider = $ad->connect();

            //dd($provider);

            // Performing a query.
            $results = $provider->search()->in('CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=gifcorporativo,DC=itu')->get();

	        //dd($results);

            // Finding a record.
            $users = $provider->search()->users()->get();

            dd($users);
            $user = $provider->search()->find('jdoe');

            // Creating a new LDAP entry. You can pass in attributes into the make methods.
            $user =  $provider->make()->user([
                'cn'          => 'John Doe',
                'title'       => 'Accountant',
                'description' => 'User Account',
            ]);

            // Setting a model's attribute.
            $user->cn = 'John Doe';

            // Saving the changes to your LDAP server.
            if ($user->save()) {
                // User was saved!
            }
        } catch (\Adldap\Auth\BindException $e) {

            // There was an issue binding / connecting to the server.

        }
        
    }
    */

    public function changePassword()
    {
        $user = $this->user->find(auth()->user()->id);
        return view('changepassword ', compact('user'));
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
        ]);
        $credentials = $request->only(
            'email',
            'password',
            'password_confirmation'
        );
        $user = \Auth::user();
        $user->password = bcrypt($credentials['password']);
        $user->save();
        return redirect('home');
    }
}
