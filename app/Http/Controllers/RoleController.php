<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Role;
use App\User;
use App\Permission;

class RoleController extends Controller
{
    public function __construct(Role $role, User $user, Permission $permission)
    {
        $this->middleware('auth');
        $this->role = $role;
        $this->user = $user;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->all();
        foreach ($users as $usr) {
            $users[$usr->id] = $usr->name;
        }
        $permissions = $this->permission->all();
        foreach ($permissions as $perm) {
            $permissions[$perm->id] = $perm->name;
        }
        $roles = $this->role->all();
        return view('roles', compact('roles'))->withUsers($users)->withPermissions($permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }

        $pms = $this->permission->all();
        foreach ($pms as $permission) {
            $permissions[$permission->id] = $permission->label;
        }
        return view('create-role')->withUsers($users)->withPermissions($permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataForm = $request->all();
        //dd($dataForm);
        //Faz a inserção dos dados
        $insert = $this->role->create($dataForm);
        $insert_users = $insert->users()->sync($request->users, false);
        //Valida operação de inserção
        if ($insert or $insert_users)
            return redirect()->route('role.index');
        else
            return redirect()->route('role.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = $this->role->find($id);
        $selected_users = $roles->users;
        $selected_permissions = $roles->permissions;
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        $pms = $this->permission->all();
        foreach ($pms as $permission) {
            $permissions[$permission->id] = $permission->label;
        }
        return view('edit-role', compact('roles', 'selected_users', 'users', 'selected_permissions', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataform = $request->all();

        $role = $this->role->find($id);
        $update = $role->update($dataform);

        $update_users = $role->users()->sync($request->users);
        $update_permissions = $role->permissions()->sync($request->permissions);

        if ($update and $update_users)
            return redirect()->route('role.index');
        else
            return redirect()->route('role.edit', $id)->with(['errors' => 'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function insert_users_read()
    {
        DB::insert('INSERT INTO role_user (user_id, role_id)  SELECT id as user_id, 1 as role_id FROM users u where u.id not in (select ru.user_id from role_user ru where ru.role_id = 1)');
        Log::info('Users was inserted to role 1...');
    }
}
