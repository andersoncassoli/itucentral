<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;

class GroupController extends Controller
{
    public function __construct(Group $group, User $user)
    {
        $this->middleware('auth');
        $this->group = $group;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->all();
        foreach ($users as $contact) {
            $contacts[$contact->id] = $contact->name;
        }
        $groups = $this->group->sortable(['name' => 'asc'])->paginate(100);
        return view('groups', compact('groups'))->withContacts($contacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        return view('create-group')->withUsers($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataForm = $request->all();
        //dd($dataForm);
        //Faz a inserção dos dados
        $insert = $this->group->create($dataForm);
        $insert_users = $insert->users()->sync($request->users, false);
        //Valida operação de inserção
        if ($insert or $insert_users)
            return redirect()->route('group.index');
        else
            return redirect()->route('group.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = $this->group->find($id);
        $selected_users = $groups->users;
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        return view('edit-group', compact('groups', 'selected_users', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataform = $request->all();

        $group = $this->group->find($id);
        $update = $group->update($dataform);

        $update_users = $group->users()->sync($request->users);

        if ($update and $update_users)
            return redirect()->route('group.index');
        else
            return redirect()->route('group.edit', $id)->with(['errors' => 'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = $this->group->find($id);
        $group->delete();
        return redirect()->route('group.index');
    }
}
