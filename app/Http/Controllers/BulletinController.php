<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Group;
use App\User;
use App\Bulletin;
use App\Permission;
use App\Role;
use App\File;
use App\Notifications\BulletinNotifications;


class BulletinController extends Controller
{
    public function __construct(Group $group, User $user, Bulletin $bulletin, Role $role, File $file)
    {
        $this->middleware('auth');
        $this->group = $group;
        $this->user = $user;
        $this->bulletin = $bulletin;
        $this->role = $role;
        $this->file = $file;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user->find(auth()->user()->id);
        $this->authorize('view_bulletin', $user->id);
        $bulletins = $this->user->find($user->id)->my_bulletins()->where('status', '=', 5)->sortable(['id' => 'desc'])->paginate(10);
        //dd($bulletins);
        $group = $this->bulletin->groups();
        //dd($details);
        return view('bulletins', compact('bulletins'))->withGroups($group);
    }

    public function requestbulletin()
    {
        $user_id = auth()->user()->id;
        $bulletins = $this->bulletin->sortable(['id' => 'desc'])->paginate(10);
        /*
        SELECT DISTINCT B.* FROM bulletins B 
        LEFT OUTER JOIN bulletin_approval A ON B.id = A.bulletin_id 
        WHERE (B.created_by = 2 OR A.user_id = 2)
        */
        //$new_bulletins = $this->user->find($user_id)->my_bulletins()->paginate(10);

        $my_bulletins = Bulletin::leftJoin('bulletin_approval', 'bulletins.id', '=', 'bulletin_approval.bulletin_id')
            ->select(DB::raw('DISTINCT bulletins.*'))
            ->where('bulletins.created_by', '=', $user_id)
            ->orWhere('bulletin_approval.user_id', '=', $user_id)
            ->sortable(['id' => 'desc'])->paginate(10);

        //$new_bulletins = $this->bulletin->sortable(['id' => 'desc'])->paginate(10);
        $group = $this->bulletin->groups();
        return view('request-bulletin', compact('bulletins', 'my_bulletins'))->withGroups($group);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gps = $this->group->all();
        foreach ($gps as $group) {
            $groups[$group->id] = $group->name;
        }
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        return view('create-bulletin')->withGroups($groups)->withUsers($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataform = $request->all();
        $note = $request->message;
        $dom = new \domdocument();
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHtml('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $note);
        // Restore error level
        /*
        libxml_use_internal_errors($internalErrors);
        $dom->encoding = 'utf-8';
        $images = $dom->GetElementsByTagName('img');

        foreach($images as $img){
			$src = $img->getAttribute('src');
	
			// if the img source is 'data-url'
			if(preg_match('/data:image/', $src)){
				
				// get the mimetype
				preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
				$mimetype = $groups['mime'];
				
				// Generating a random filename
				$filename = uniqid();
				$filepath = "/app/public/$filename.$mimetype";
	
				// @see http://image.intervention.io/api/
				$image = Image::make($src)

                  
				  ->encode($mimetype, 100) 	// encode file to the specified mimetype
				  ->save(storage_path($filepath));
				
				$new_src = asset($filepath);
				$img->removeAttribute('src');
				$img->setAttribute('src', $new_src);
			} // <!--endif
        } // <!--endforeach
        */

        $message = $dom->saveHTML($dom->documentElement);

        $date = Carbon::createFromFormat('d-m-Y', $request->date_post);
        $priority = head($dataform['priorities']);
        //$this->bulletin->message = strip_tags($message,'<br><p><img><table><a><td><tr><span><li><ul>');
        $this->bulletin->message = $message;

        /*
        if ($image = $request->file('image')) {
            foreach ($image as $files) {
            $destinationPath = 'public/image/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $insert[]['image'] = "$profileImage";
            }
        }
        */

        /*
        if ($request->file('file')) {
            //$uploadedFile = $request->file('file')->store('files');
            $uploadedFile = $request->file('file')->store('storage');
            $filename = $request->file('file')->getClientOriginalName();
            $url = Storage::url($uploadedFile);
        } else {
            $uploadedFile = null;
            $filename = null;
            $url = null;
        }
        */

        // Faz a inserção dos dados
        $this->bulletin->subject = $request->subject;
        $this->bulletin->priority = $priority;
        $this->bulletin->message = $request->message;
        $this->bulletin->date_post = $date;
        $this->bulletin->status = 0;
        $this->bulletin->owner()->associate(auth()->user()->id);
        $this->bulletin->area = auth()->user()->ou_name;

        // Salva comunicado
        $insert = $this->bulletin->save();

        $bulletin = $this->bulletin;

        foreach ($request->input('file', []) as $file) {
            //dd($file);
            $new_file = $this->file->find($file);
            $new_file->bulletin()->associate($bulletin);
            $new_file->save();
        }
        /*
        if ($files = $request->file('file')) {
            foreach ($files as $fl) {
                $uploadedFile = $fl->store('storage');
                $filename = $fl->getClientOriginalName();
                $url = Storage::url($uploadedFile);
                $file = new File;
                $file->file_name = $uploadedFile;
                $file->file_url = config('app.url').'/'.$uploadedFile;
                $file->file_original_name = $filename;
                $this->bulletin->files()->save($file);
            }
        }
        */
        // Associa os grupos e usuários
        $insert_groups = $this->bulletin->groups()->sync($request->groups, false);
        foreach ($request->groups as $grp) {
            $uss = $this->group->find($grp)->users;
            foreach ($uss as $u) {
                $usr = $this->user->find($u->id);
                $insert_my_groups = $usr->my_bulletins()->sync($this->bulletin, false);
            }
        }

        if ($request->users) {
            $insert_users = $this->bulletin->users()->sync($request->users, false);
            foreach ($request->users as $u) {
                $usr = $this->user->find($u);
                $insert_my_users = $usr->my_bulletins()->sync($this->bulletin, false);
            }
        } else {
            $insert_users = true;
            $insert_my_users = true;
        }

        $users_ed = User::whereHas('roles', function ($q) {
            $q->where('name', 'Edição');
        })->get();

        foreach ($users_ed as $user) {
            $user->notify(new BulletinNotifications($bulletin, 'create'));
        }


        //Valida operação de inserção
        //if( $insert and $insert_groups and $insert_users and $insert_my_groups and $insert_my_users)
        if ($insert and $insert_groups and $insert_users and $insert_my_users)
            return redirect()->route('requestbulletin')->with('message', 'Solicitação de comunicado enviada com sucesso!');
        else
            return redirect()->route('bulletin.create')->with(['errors' => 'Falha ao atualizar status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = new Carbon();
        // Recuper item
        $bulletin = $this->bulletin->find($id);

        // Autoriza acesso
        $this->authorize('view_bulletin');

        // Recuperar o grupo, usuários, solicitante, status e anexo
        $group = $bulletin->groups;
        $users = $bulletin->users;
        $owner = $bulletin->owner;
        $files = $bulletin->files;
        $user = $this->user->find(auth()->user()->id);
        //$url = config('app.url').'/'.$bulletin->file_name;
        $user->my_bulletins()->sync([$bulletin->id => ['read_at' => $now]], false);
        //$bulletin->approvals()->sync([$user->id => [ 'acceptyn' => 1] ], false);

        // Chama a view
        return view('show-bulletin', compact('bulletin', 'group', 'users', 'owner', 'files'));
    }

    public function view($id)
    {
        // Recuper item
        $bulletin = $this->bulletin->find($id);

        // Autoriza acesso
        $this->authorize('view_bulletin');

        // Recuperar o grupo, usuários, solicitante, status e anexo
        $group = $bulletin->groups;
        $users = $bulletin->users;
        $owner = $bulletin->owner;
        $files = $bulletin->files;
        $user = $this->user->find(auth()->user()->id);
        //$url = config('app.url').'/'.$bulletin->file_name;
        //$bulletin->approvals()->sync([$user->id => [ 'acceptyn' => 1] ], false);

        // Chama a view
        return view('show-bulletin', compact('bulletin', 'group', 'users', 'owner', 'files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bulletin = $this->bulletin->find($id);
        $grps = $this->group->all();
        foreach ($grps as $group) {
            $groups[$group->id] = $group->name;
        }
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        $files = $bulletin->files;
        $selected_groups = $bulletin->groups;
        $selected_users = $bulletin->users;
        $selected_usersapp = $bulletin->approvals;
        //$owner_name = $bulletin->owner->name;
        $dt = new Carbon($bulletin->date_post);
        $dt_string = $dt->format('d-m-Y');
        $auth = auth()->user();
        if ($bulletin->status == 0) {
            $bulletin->status = 1;
        }

        $roles = $this->role->find(4);
        $usr_app = $roles->users->all();
        foreach ($usr_app as $upp) {
            $users_app[$upp->id] = $upp->name;
        }

        $update = $bulletin->save();
        //dd($files);

        if (isset($bulletin->copied_from)) {
            return view('resend-bulletin', compact('bulletin', 'groups', 'users', 'users_app', 'selected_usersapp', 'dt_string', 'auth'));
        } else {
            return view('edit-bulletin', compact('bulletin', 'groups', 'users', 'users_app', 'selected_groups', 'selected_users', 'selected_usersapp', 'dt_string', 'files', 'auth'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataform = $request->all();

        $note = $request->message;

        $dom = new \domdocument();
        //dd($note);
        // set error level
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHtml('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $note);
        // Restore error level
        /*
        libxml_use_internal_errors($internalErrors);
        $images = $dom->getElementsByTagName('img');

        // foreach <img> in the submited message
		foreach($images as $img){
			$src = $img->getAttribute('src');
	
			// if the img source is 'data-url'
			if(preg_match('/data:image/', $src)){
				
				// get the mimetype
				preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
				$mimetype = $groups['mime'];
				
				// Generating a random filename
				$filename = uniqid();
				$filepath = "/app/public/$filename.$mimetype";
	
				// @see http://image.intervention.io/api/
				$image = Image::make($src)

                  
				  ->encode($mimetype, 100) 	// encode file to the specified mimetype
				  ->save(storage_path($filepath));
				
				$new_src = asset($filepath);
				$img->removeAttribute('src');
				$img->setAttribute('src', $new_src);
			} // <!--endif
        } // <!--endforeach
        */

        $message = $dom->saveHTML();

        $bulletin = $this->bulletin->find($id);

        //$group = $this->group->find(head($dataform['groups']));

        //$dataform['message'] = strip_tags($message,'<br><p><img><table><a><td><tr><span><li><ul>');
        $dataform['message'] = $message;
        $dataform['date_post'] = Carbon::createFromFormat('d-m-Y', $request->date_post);
        $dataform['status'] = head($request->status);
        $dataform['priority'] = head($request->priority);

        //$bulletin['message'] = $message;
        //dd($bulletin['message']);
        $update = $bulletin->update($dataform);

        /*
        if ($files = $request->file('file')) {
            foreach ($files as $fl) {
                $uploadedFile = $fl->store('storage');
                $filename = $fl->getClientOriginalName();
                $url = Storage::url($uploadedFile);
                $file = new File;
                $file->file_name = $uploadedFile;
                $file->file_url = config('app.url').'/'.$uploadedFile;
                $file->file_original_name = $filename;
                $bulletin->files()->save($file);
            }
        }
        */

        $files = $bulletin->files->all();
        foreach ($files as $file) {
            $fl = $this->file->find($file->id);
            $fl->bulletin()->dissociate($bulletin);
            $fl->save();
        }

        foreach ($request->input('file', []) as $file) {
            //dd($file);
            $new_file = $this->file->find($file);
            $new_file->bulletin()->associate($bulletin);
            $new_file->save();
        }

        $update_groups = $bulletin->groups()->sync($request->groups);
        //$insert_my_groups = $this->bulletin->groups()->sync($request->groups, false);

        // Exclui usuários relacionados ao comunicado para sincronizá-los a seguir
        DB::table('my_bulletins')->where('bulletin_id', '=', $id)->delete();

        // Percorre os grupos extraindo os usuários para associá-los na tabela my_bulletins
        foreach ($request->groups as $grp) {
            $uss = $this->group->find($grp)->users;
            foreach ($uss as $u) {
                $usr = $this->user->find($u->id);
                $update_my_groups = $usr->my_bulletins()->sync($bulletin, false);
            }
        }

        //dd($insert_my_groups);

        $update_users = $bulletin->users()->sync($request->users);

        //$insert_users = $this->bulletin->users()->sync($request->users, false);
        if ($request->users) {
            foreach ($request->users as $u) {
                $usr = $this->user->find($u);
                $update_my_users = $usr->my_bulletins()->sync($bulletin, false);
            }
        } else {
            $update_my_users = true;
        }

        $update_usersapp = $bulletin->approvals()->sync($request->users_app);

        if ($update and $update_groups and $update_users and $update_my_groups and $update_my_users and $update_usersapp)
            return redirect()->route('bulletin.show', $id);
        else
            return redirect()->route('bulletin.edit', $id)->with(['errors' => 'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bulletin = $this->bulletin->find($id);

        $bulletin->groups()->dissociate();

        $delete = $bulletin->delete();

        if ($delete)
            return redirect()->route('bulletin.index');
        else
            return redirect()->route('bulletin.index')->with(['errors' => 'Falha ao excluir']);
    }

    /*
    // Não será mais utilizado. #Remover#
    public function sendtest(Request $request)
    {
        $dataform = $request->all();
        set_time_limit(0);
        $to = $request->email;
        $bulletin = $this->bulletin->find($request->id);
        $dataform['status'] = head($request->status);
        $dataform['priority'] = head($request->priority);
        $update = $bulletin->update($dataform);
        $bulletin->message = strip_tags($bulletin->message,'<br><p><img><table><a><td><tr><span><li><ul>');
        Mail::to($to)->send(new BulletinMail($bulletin));
        return redirect()->route('bulletin.edit', $request->id)->with('message', 'Comunicado '.$request->id.' enviado para teste.');
    }
    */

    /*
    Marcado para remoção
    public function sendbulletin(Request $request)
    {
        $dataform = $request->all();

        $note = $request->message;

        $dom = new \domdocument();
        //dd($note);
        // set error level
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHtml('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $note);
        // Restore error level
        libxml_use_internal_errors($internalErrors);
        $images = $dom->getElementsByTagName('img');

        // foreach <img> in the submited message
        foreach ($images as $img) {
            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if (preg_match('/data:image/', $src)) {

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = uniqid();
                $filepath = "/app/public/$filename.$mimetype";

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                    // resize if required


                    ->encode($mimetype, 100)     // encode file to the specified mimetype
                    ->save(storage_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!--endforeach

        $message = $dom->saveHTML();

        $bulletin = $this->bulletin->find($request->id);

        if ($request->file('file')) {
            $file = $request->file->store('file');
            $uploadedFile = $request->file('file')->store('public');
            $filename = $request->file('file')->getClientOriginalName();
            $url = Storage::url($uploadedFile);
            $bulletin->file_name = $uploadedFile;
            $bulletin->file_url = $url;
            $bulletin->file_original_name = $filename;
        };

        //$group = $this->group->find(head($dataform['groups']));

        //$dataform['message'] = strip_tags($message,'<br><p><img><table><a><td><tr><span><li><ul>');
        $dataform['message'] = $message;
        $dataform['date_post'] = Carbon::createFromFormat('d-m-Y', $request->date_post);
        $dataform['status'] = 1;
        $dataform['priority'] = head($request->priority);

        //$bulletin['message'] = $message;
        //dd($bulletin['message']);
        $update = $bulletin->update($dataform);

        $update_groups = $bulletin->groups()->sync($request->groups);
        $update_users = $bulletin->users()->sync($request->users);

        if ($update)
            return redirect()->route('bulletin.index')->with('message', 'Comunicado ' . $request->id . ' foi enviado com sucesso!');
        else
            return redirect()->route('bulletin.show', $request->id)->with(['errors' => 'Falha ao enviar o comunicado' . $request->id . '...']);
    }
    */

    public function sendapproval(Request $request)
    {
        $dataform = $request->all();
        set_time_limit(0);

        $bulletin = $this->bulletin->find($request->id);
        //dd($bulletin->users);
        //$grp = $this->group->find(8);
        //$roles = $this->role->find(4);
        //$users_app = $roles->users;


        foreach ($bulletin->approvals as $user) {
            $user->notify(new BulletinNotifications($bulletin, 'approval'));
        }


        //$gps = $this->group->all();
        /*
        foreach ($bulletin->groups as $group) {
            $grp = $this->group->find($group->id);
            $users = $grp->users->where('approval', '=', 1);  
        }

        $dataform['status'] = head($request->status);
        $dataform['priority'] = head($request->priority);
        $update = $bulletin->update($dataform);
        
        $bulletin->message = strip_tags($bulletin->message,'<br><p><img><table><a><td><tr><span><li><ul>');
        */
        //dd($bulletin);


        $bulletin->status = 2;
        $update = $bulletin->save();
        if ($update)
            return redirect()->route('requestbulletin')->with('message', 'Comunicado ' . $request->id . ' enviado para aprovação.');
        else
            return redirect()->route('bulletin.show')->with(['errors' => 'Falha ao atualizar status']);
    }

    public function bulletinno(Request $request)
    {
        $bulletin = $this->bulletin->find($request->id);
        $user = $this->user->find(auth()->user()->id);
        $users_app = $bulletin->approvals;
        $bulletin->approvals()->sync([$user->id => ['acceptyn' => 0, 'comment' => $request->comment]], false);

        $bulletin->status = 3;
        //$bulletin->comment = $request->comment;        
        $update = $bulletin->save();

        // Se quiserem que envie os e-mail, descomentar
        /*
        $owner_mail = $bulletin->owner->email;
        $msg = 'O comunicado '. $request->id .' foi rejeitado.';
        $subj = 'Comunicado '. $request->id . ' - Notificação';

        $roles = $this->role->find(3);
        $users = $roles->users->all();
        
        foreach ($users as $user) {
            $to = $user->email;
            Mail::raw($msg, function ($message) use ($subj, $to) {
            $message->to($to);
            $message->subject($subj);
        });
        }

        Mail::raw($msg, function ($message) use ($subj, $owner_mail) {
            $message->to($owner_mail);
            $message->subject($subj);
        });

        */
        $users_ed = User::whereHas('roles', function ($q) {
            $q->where('name', 'Edição');
        })->get();

        foreach ($users_ed as $user) {
            $user->notify(new BulletinNotifications($bulletin, 'reject'));
        }

        if ($update)
            return redirect()->route('bulletin.index')->with('message', 'Comunicado ' . $request->id . ' Rejeitado!');
        else
            return redirect()->route('bulletin.index')->with(['errors' => 'Falha ao atualizar status']);
    }

    public function bulletinyes(Request $request)
    {
        $dataform = $request->all();
        $now = new Carbon();
        $bulletin = $this->bulletin->find($request->id);
        $user = $this->user->find(auth()->user()->id);
        $users_app = $bulletin->approvals;
        $bulletin->approvals()->sync([$user->id => ['acceptyn' => 1, 'comment' => $request->comment]], false);
        $uapp_tot = $bulletin->approvals->count();
        $sum_app = $bulletin->approvals()->sum('acceptyn');
        //$sum = $users_app->pivot->sum('acceptyn');
        /*
        dd($count_app);
        foreach ($users_app as $user_app) {
            $count_app = $user_app->pivot->acceptyn + $count_app;
        }
        */
        if ($uapp_tot == $sum_app) {
            $bulletin->status = 4;
            $update = $bulletin->save();
        } else {
            $update = true;
        }
        //$bulletin->status = 4;


        // Se quiserem que envie os e-mail, descomentar
        /*
        $owner_mail = $bulletin->owner->email;
        $msg = 'O comunicado '. $request->id .' foi aprovado!';
        $subj = 'Comunicado '. $request->id . ' - Notificação';

        $roles = $this->role->find(3);
        $users = $roles->users->all();
        
        foreach ($users as $user) {
            $to = $user->email;
            Mail::raw($msg, function ($message) use ($subj, $to) {
            $message->to($to);
            $message->subject($subj);
        });
        }

        Mail::raw($msg, function ($message) use ($subj, $owner_mail) {
            $message->to($owner_mail);
            $message->subject($subj);
        });

        //dd($owner_mail);
        */
        $users_ed = User::whereHas('roles', function ($q) {
            $q->where('name', 'Edição');
        })->get();

        foreach ($users_ed as $user) {
            $user->notify(new BulletinNotifications($bulletin, 'accept'));
        }

        if ($update)
            return redirect()->route('bulletin.index')->with('message', 'Comunicado ' . $request->id . ' Aprovado!');
        else
            return redirect()->route('bulletin.index')->with(['errors' => 'Falha ao atualizar status']);
    }


    public function acceptall($id)
    {
        $bulletin = $this->bulletin->find($id);
        $user = $this->user->find(auth()->user()->id);
        $users_app = $bulletin->approvals;
        foreach ($users_app as $uapp) {
            //dd($uapp->id);
            $bulletin->approvals()->sync([$uapp->id => ['acceptyn' => 1, 'comment' => 'Aprovação autorizado por ' . $user->name]], false);
        }
        $bulletin->status = 4;
        $update = $bulletin->save();
        if ($update)
            return redirect()->route('requestbulletin')->with('message', 'Comunicado ' . $id . ' Aprovado!');
        else
            return redirect()->route('requestbulletin')->with(['errors' => 'Falha ao atualizar status']);
    }

    public function sendpublish(Request $request)
    {
        $dataform = $request->all();
        $now = new Carbon();
        set_time_limit(0);
        $to = 'noreply@itucentral.com.br';
        $bulletin = $this->bulletin->find($request->id);

        $bulletin->status = 5;
        $update = $bulletin->save();
        //$bulletin->message = strip_tags($bulletin->message,'<br><p><img><table><a><td><tr><span><li><ul>');
        //$group = Group::where('id', $bulletin->group_id)->get()->first();
        /*
        foreach ($bulletin->groups as $group) {
            $grp = $this->group->find($group->id);
            $grp_users = $grp->users->where('approval', '=', 1);  
        }

        foreach ($grp_users as $g_usr) {
            //dd($usr->email);
            Mail::to($to)
                ->bcc($g_usr->email)
                ->send(new BulletinMail($bulletin));
        }
        */

        foreach ($bulletin->my_users as $user) {
            $user->notify(new BulletinNotifications($bulletin, 'publish'));
        }

        //dd($bulletin);

        if ($update)
            return redirect()->route('requestbulletin')->with('message', 'Comunicado ' . $request->id . ' publicado com sucesso!');
        else
            return redirect()->route('requestbulletin')->with(['errors' => 'Falha ao enviar comunicado']);
    }

    public function resend($id)
    {
        $bulletin_old = $this->bulletin->find($id);

        $bulletin = $bulletin_old->replicate();
        $bulletin->push();

        $bulletin->approvals()->sync($bulletin_old->approvals, false);

        $bulletin->push();

        $bulletin->status = 4;
        //$bulletin->copied_from == $id;
        $bulletin->copied()->associate($id);

        $update = $bulletin->update();
        //dd($files);
        return redirect()->route('bulletin.edit', $bulletin->id);
    }

    public function printpdf($id)
    {
        $bulletin = $this->bulletin->find($id);
        /*
        $dt = new Carbon($bulletin->date_post);
        $dt_string = $dt->format('d/m/Y');
        // This  $data array will be passed to our PDF blade
        $data = [
            'title' => $bulletin->subject,
            'heading' => 'Comunicado:'.$bulletin->id.' | '.$dt_string,
            'content' => $bulletin->message,
            ];
        $name = $bulletin->id.'-'.$bulletin->subject;
        $pdf = PDF::loadView('pdf_view', $data);
        return $pdf->stream($name);
        */
        $bulletin = $this->bulletin->find($id);
        $dt = new Carbon($bulletin->date_post);
        $dt_string = $dt->format('d-m-Y');
        $mpdfConfig = array(
            'mode' => 'utf-8',
            'format' => 'A4',
            'default_font' => 'Arial',
            'margin_header' => 10,     // 30mm not pixel
            'margin_footer' => 10,     // 10mm
            'orientation' => 'P',
            'setAutoTopMargin' => 'stretch',
        );
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $mpdf->SetHTMLHeader('
            <div style="background-image: url(img/1x/header_prt_new_001.png); width:678px; height:230px;">
                <table style="width:100%; height:100px; top:20px;">
                    <tr>
                        <td style="width:50%; align:center; vertical-align: middle; text-align: center; font-size:18px; color:white;">Data: ' . $dt_string . '</td>
                        <td style="width:50%; align:center; vertical-align: middle; text-align: center; font-size:18px; color:white;">N&ordm; ' . $bulletin->id . '</td>
                    <tr>
                </table>
                <div style="position:absolute; width:100%; height:105px; vertical-align: middle; text-align: center; font-size:10px;">
                    <table style="width:100%;">
                        <tr>
                            <td>&nbsp;</td>
                        <tr>
                        <tr>
                            <td>&nbsp;</td>
                        <tr>
                        <tr>
                            <td>&nbsp;</td>
                        <tr>
                    </table>
                </div>
                <div style="position:absolute; width:100%; vertical-align: middle; text-align: center; ">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:100%; align:center; vertical-align: middle; text-align: center; font-size:18px; color: #676A6C;">' . $bulletin->subject . '</td>
                        <tr>
                    </table>
                </div>
            </div>');
        $mpdf->SetHTMLFooter('
            <div style="background-image: url(img/1x/footer_prt_new_001.png); width:678px; bottom:0px;">
                <table style="width:100%; align:center; vertical-align: middle; text-align: center;">
                    <tr>
                        <td style="width:88%; text-align:center; color:white;">' . $bulletin->area . '</td>
                        <td style="width:12%; text-align:center;">{PAGENO}/{nbpg}</td>
                    </tr>
                </table>
            </div>');
        // Exibir data impressa no comunicado
        // <td width="50%">{DATE j-m-Y}</td>
        $mpdf->WriteHTML($bulletin->message);
        $pdf_name = 'IC-Comunicado-' . $bulletin->id . '-' . $bulletin->subject . '.pdf';

        $mpdf->Output($pdf_name, 'I');
    }

    /*
    Marcado para remoção
    public function removefile($id)
    {

        $file = $this->file->find($id);
        $bulletin = $file->bulletin;
        $delete = $file->delete();
        if ($delete)
            return redirect()->route('bulletin.edit', $bulletin->id);
        else
            return redirect()->route('bulletin.edit', $bulletin->id)->with(['errors' => 'Falha ao excluir arquivo']);
    }
    */
    /*
    Marcado para remoção
    public function inputcomment(Request $request)
    {
        dd($request);

        $bulletin = $this->bulletin->find($request->id);
        $bulletin->comment = $request->comment;
        $bulletin->save();
        return redirect()->route('bulletin.show', $id);
    }
    */

    public function test($id)
    {
        //$path = auth()->user()->approval;
        /*
        $users = Group::where('id', '=', 5)
            ->with('users')
            ->get();
        foreach ($users as $usr) {
            $email[$usr->id] = $usr->email;
        }
        $emails = $users->Group->User->email;
        */
        /*
        $group = Group::where('id', 5)->get()->first();
        $users = $group->users;
        foreach ($users as $usr) {
            $emails[$usr->id] = $usr->email;
        }
        dd($emails);
        */
        //$recipients = explode(',', $emails);
        //dd($recipients);
        /*
        $gps = $this->group->all();
        foreach ($gps as $group) {
            $groups[$group->id] = $group->name;
        }
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        */
        /*
        $bulletin = $this->bulletin->find(54);

        $r = $bulletin->groups->all();
        foreach ($r as $r2) {
            $g = $this->group->find($r2->id);
            $u = $g->users->all();
            foreach ($u as $u2) {
                $x = $this->user->find($u2->id);
                $users[$x->id] = $x->id;
            }
        }

        dd($users);
        */

        //dd($x);

        /*
        // Recuperar o grupo, usuários, solicitante, status e anexo
        $group = $bulletin->groups;
        $users = $bulletin->users;
        $owner = $bulletin->owner;
        $user_auth = auth()->user()->approval;
        $url = config('app.url').'/'.$bulletin->file_name;

        // Chama a view
        return view('test', compact('bulletin', 'group', 'users', 'owner', 'user_auth', 'url'));
        //return view('test')->withGroups($groups)->withUsers($users);
        //return view('test');
        */
        /*
        $bulletin = $this->bulletin->find(89);
        $sendmail = Mail::to('anderson.cassoli@mobitap.com.br')->send(new BulletinMail($bulletin));
        dd($bulletin);
        */

        //$user_id = auth()->user()->id;
        //$user = $this->user->find($user_id);
        //$bull = $user->my_bulletins->all();
        //$bulletin = $this->bulletin->find(55);

        //dd($bulletin);

        //$update_users = $bulletin->users()->sync($request->users);
        //$update = $user->my_bulletins()->sync($bulletin);
        //dd($update);

        //$bulletins = $this->user->find($user_id)->my_bulletins()->paginate(10);
        //$group = $this->bulletin->groups();
        //dd($bulletins);
        //return view('bulletins', compact('bulletins', 'user_id'))->withGroups($group);
        //dd(storage_path());
        //$this->authorize('approve_bulletin');
        //$permissions = Permission::where('id', '=', '3')->with('roles')->get();
        //$roles = $this->role->find(4);
        //$users = $roles->users->all();

        //$user = User::where('id', '=', auth()->user()->id)->with('roles')->get();

        //$user = $this->user->find(auth()->user()->id)->with('roles');
        /*
        foreach ($users as $user) {
            dd($user);
        }
        */


        //dd($users);
        /*
        $gps = $this->group->all();
        foreach ($gps as $group) {
            $groups[$group->id] = $group->name;
        }
        $uss = $this->user->all();
        foreach ($uss as $user) {
            $users[$user->id] = $user->name;
        }
        */
        /*
        $bulletin = $this->bulletin->find(2);
        $files = $bulletin->files;

        return view('test', compact('files'));
        */
        /*
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', 'Edição');
        })->get();

        dd($users);
        */
        //$to = 'noreply@itucentral.com.br';
        //$bulletin = $this->bulletin->find($id);

        //dd($bulletin->my_users->all());

        //foreach ($bulletin->my_users as $usr) {
        //dd($usr->email);
        //$user = $this->user->find(568);
        // SendEmail::dispatch($user, $bulletin);
        //$user->notify(new BulletinNotifications($bulletin));

        /*
            Mail::to($to)
                ->bcc($usr->email)
                ->send(new BulletinMail($bulletin));
            */
        //}

        //dd($user);
        $user = $this->user->find($id);
        $user->delete();
        return redirect()->route('user.index');
    }

    public function test_store(Request $request)
    {

        $bulletin = $this->bulletin->find(2);
        dd($request->all());

        $files = $bulletin->files->all();
        foreach ($files as $file) {
            $fl = $this->file->find($file->id);
            $fl->bulletin()->dissociate($bulletin);
            $fl->save();
        }

        //$bulletin->save();
        //DB::table('files')->where('bulletin_id', '=', $bulletin->id)->delete();

        foreach ($request->input('file', []) as $file) {
            //dd($file);
            $new_file = $this->file->find($file);
            $new_file->bulletin()->associate($bulletin);
            $new_file->save();
        }

        dd($new_file);
    }
}
