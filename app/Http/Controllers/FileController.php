<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function __construct(File $file)
    {
        $this->middleware('auth');
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ('OK');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = storage_path('files');
        
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $uploadedFile = $file->store('storage');
        $filename = $file->getClientOriginalName();
        $file_size = $file->getSize();
        $file_type = $file->getMimeType();
        $url = Storage::url($uploadedFile);

        $this->file->file_name = $uploadedFile;
        $this->file->file_url = config('app.url').'/'.$uploadedFile;
        $this->file->file_original_name = $filename;
        $this->file->file_size = $file_size;
        $this->file->file_type = $file_type;

        $insert_file = $this->file->save();

        $file_id = $this->file->id;

        return response()->json([
            'id'            => $file_id,
            'name'          => $uploadedFile,
            'original_name' => $filename,
            'url'           => config('app.url').'/'.$uploadedFile,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
