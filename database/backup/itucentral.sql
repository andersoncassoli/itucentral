-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Jul-2019 às 20:36
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itucentral`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bulletins`
--

CREATE TABLE `bulletins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_post` datetime NOT NULL,
  `date_email_sent` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_original_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `bulletins`
--

INSERT INTO `bulletins` (`id`, `subject`, `priority`, `message`, `date_post`, `date_email_sent`, `status`, `group_id`, `created_at`, `updated_at`, `approved`, `file_name`, `file_url`, `file_original_name`, `comment`) VALUES
(13, 'Informações de 2019', 2, '<blockquote class=\"blockquote\"><p>Comunicado 1 - Teste</p><ul><li>Item 1</li><li>Item 2</li><li><span style=\"background-color: rgb(255, 255, 0);\">Item 3</span></li></ul></blockquote>', '2019-07-05 00:00:00', NULL, 3, 5, NULL, '2019-06-26 23:37:56', 0, 'files/f2hYzArnXwnTSTeGBfKODihm3oqgvWDFzjM1qAIO.pdf', '/storage/files/f2hYzArnXwnTSTeGBfKODihm3oqgvWDFzjM1qAIO.pdf', 'teste (1).pdf', NULL),
(14, 'Comunicado Teste 2', 1, 'Testando envio de mensagem...', '2019-06-28 00:00:00', NULL, 2, 6, NULL, '2019-06-27 14:39:43', 0, NULL, NULL, NULL, NULL),
(15, 'Testando comunicado 3', 1, 'Teste comunicado... testando...', '2019-07-06 00:00:00', '2019-06-26 22:15:43', 4, 5, NULL, '2019-06-27 01:15:46', 0, NULL, NULL, NULL, NULL),
(16, 'Comunicado teste 2', 2, '<blockquote class=\"blockquote\"><p>Mensagem</p><h5><h2><ol><li style=\"text-align: center; \">Item 1textãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãote<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAloAAADKCAMAAABKfB6yAAADAFBMVEVMaXHOzc3k4+Oml3/s6+ve3t7R0dFmZmZVTkTt7e37+/v5+fn29vbo6OjY2Njg39/W1dXJyMj19fXm5ubb2trT0tLGxcXi4uLc3Nza2dnh4eHU1NTMy8vAv7/+/v75+fmml3+ml3/FxMTDwsK5uLju7u7q6en19fXq6urn5+fl5OT19fXy8vLw8PB7cF7p6enPzs76+vrX1tbHxsbKysq2tbX09PS7urrCwcEIBwW+vb0SEQ4HBwW9vLylgF7v7+9NTkfd3Nywr68ODQqxjWq+vb2zsrLMy8vv7+/y8vKFdk7n5+cVFA8RDwwoKCezsbEKCQYODQrCwcGomYELCwcPDQkqKikZGRkaGhpDQ0JvZVQyMjHJyMixsLCura01NTSFdk40Lh5/cUqFdk4oKCdHPykPDgxCQkG5uLg0Lh4qJRg2MB8eGxItLS0kJCMUFBQ6OjooKCezsrJ/eXCrnodqXj5qXj5AOSgXFA1EPShYTjMODQgfHBQZGBUoKCccHBw8PDslJSU1NTTa2dnR0dHU09NXVEyzsrKzpZHOzs7BwMBpXT1kWDp6bEdVSzFRRy9sYlGYjXpIQzoaGho5OThDQ0I3NzfMy8uura3KycnX1tbBwMC7urqCfHO0s7PHx8duYkCFdk5qXj51Z0Q4MiFvY0F1Z0RkWTo1Lx9/cUpvY0FSSC9SSDNMRCwZGRksLCxAQD8cHBsnJyY3Nzfb29t4cmdST0d6dGp8dmzj4+Pf399pYFDBwMBfXFRiX1isq6teWU1HPyl/cEpgVTiFdk5QSkBAQD8bGxt1almml3+9sqHt7e3u7u6vi2hdWVJqYVOop6evrq5xZEJdUjZ1Z0QyMjF1Z0SQhnaUdVh5cGA4ODcxKxw3MyjHwrz6+vqml39mZmZ7cF5QSDs8Q0KFdk41MSdIQTVBOzA5NCpGRT89Ny1EPjJMRDhPRzuskXNTSz1zaVikk3pJQzeDcl1UVEyLf2p/dGODc15DQ0I2MilxZ1Y3MypvZVRPRzpHQTU5NSq4AdQMAAAA33RSTlMAQSSAGSs8EEwXBAIJHjMpNkcDITA6SyYuMSc4RFMBB/AwTU9cFhwLGyAjDRASYB0/BjVJRWAOWVEDViYGV5sU+C0aFcYVZEMVEf4fIxqMBgoSKPAND4Q/ZvTdoARma6b6RN/ycm4e2C5HNk0rlGA1wJklvzKkv1kXZ5QMMDp/WtNqshIWI4gyNjQerKHRiYDtTFVMtummKjUaK0JJwlA8yOi5wVSxzaVe6cCMcXhDis1Iea0muoW7vQ4P4SGMjlnzYtKW10/kVeJwKgEBuYrlDla4m8WN0y6iX69AajQBpkWPfgAAAAlwSFlzAAALEgAACxIB0t1+/AAACfFJREFUeJzt3Hl0VFcdwPELkipMaMsAQlvKViBQoBg0xTSpWFNjjQlJJBvNhIQEEtlCwLITFkFlRxZRtlJo2Xeo1Vbsvrhrta7/xIHb1r1qEdG6nTf7cl/mJTPp4c39fs7h8JKZyWF+93tm3gxzIwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANBh5n2V2aIjzJtMWugI83KrSAsdYF5Gbi5pIfHmDcnIyP0ig0WiXRwyZEhGBmkh0S7eO85oi7SQYOtvv8fT1mcYLBJq/Ydv97ZFWkio9YPv8LVFWkikXe8f7G+LtJBAu3p8MNAWaSFxdvXrEWyLtJAwuzr1C2nr0wwWCbLmA51C2yItJMiaG24Ia4u0kBhrevcOb4u0kBBrbrstoi3SQiKsHjs2si3SQgKsvuWWqLZIC/Fb3bdXdFukhbg9ldJX0dZFBos4PZWSomjrK4wVcXry1lsVbVEW4vVk5z6KtigL1hT+2OxqaXd2VrT1eeYKSwqf+ZjJ1dJG9FS0RVmwpvCZySZppXX/lKItyoI1ha9ONkkrddgYRVuUBWsKX60ySSt1+HBFW2OYKywpfDi3qkqZVmqXLoq2hjFWWFL4cEZublWVIq0BQ7sq2hrOWGFJ4d6MDKOt6LQGjH9A0VaX9o+1ZGY2a6KPwr3Gb2/IyM2NSit99HhFW13jmMxP5E91H7dGXHvHjfO2FZlW+qhPKtqKpyxxQD6i+7z14dpzz72+tiLS6jZqlKKtoXFNZuEj03UfuDZcezy7oD1thafVbeTdirbGt3cwM+YHo1q4cZbuc09+rj2+HfZGW2FpdRs5UtHW6HZPZIGs8x+W1sgjug8+6bn2D74j2FZoWs60NEVb7S9LbJSH/YfZtXKz7pNPdq79xoavQFsfD95dZ2qqoq1R7ZtHVpEQomh6iRDiuQ0zhRA5U4y3IErydZ+/rdzVln+sa793M6G/rWBazgGpirbaWdYWWVvkO5wiZZ7/NGvRsbzFtp+3RtxtaMu17kM9wtoKpOVIH6Bo6+52jnGBlP6cTkopH/MdT5Fyvu7LZSdu62251n2iX3hb/rQc6emKtka2dwwLDxzKFEIYf2bVyLoC3/HS1w5MSabRJzu35bZc6zp1imjLl5ajW7qirbQ4J7cx73iJEMtOZQlRsCmP03jbcVtty7XO2DcR3pY3rf7Oboq24i1rmZRymu/4hJR5i5Jp6lpwW2zLdZ93T05YW560+jucirZS4x3eLE9aJafmT8sWm6WsydF9pWzHba0t5+7evaPb8qTlcCjaiq8s48xKvFB3qKjo51LKRzNzjtdtCXwfNuG21JZz9803K9oy0urfP7qt7w6I684/V/OoL6LF0jDT+0XpprqZZGUfbittOXePHatqy5tWVFtrJ8Z39xdImeU9OuFJ66T3C958sBe3hbacj994o7It77lWZFtrJ7bEN4LFef4P0kwLfdQqqq3hUctG3LHbcj7eq5e6Ld8rxPC21k5sMU9r0HesjCbHeD489eKS7NJaKeXh7KVHXjTeh88uTuKFSD7umG0577yvr7qtfT/yXiOsLaMs07QGDXyP1Qlm5Um5WCxasmBzqdgiZQ1Z2Y07VlvOnikp6rb2XXV/03udkLYeMsoyS2vQwBbLac2QUp7yHS+RUhYk9TIkI3eMthwjbkpRt7Xvqtsd1dZDA1vM0xo0sMV6WuKg3JQjRP3RHCHyazmBtx936205uve5Sd3WZ696bvGg93q+tnxlqdMyyrKU1vTnXzD+Mt5/z6+Tx4qFyDSOszdsmqHV2ticu9W2HMM691G35SsrvC1/Wcq0PGVZSiv45oPnJWLIJx826rlI9uRurS3HcGOzl6qtQFmhbQXKUqXlLctSWtPyDvs3H87IkzX+DwAW19Y81sqtcJ2JTCu0LUeXnj3VbYWUFWxrUKAsRVr+Cy2da5V4SjpqnLlPX9JsPC8eXWr8P89S6rGRqLSCbTm6dh+hbiusLH9bIWVFpxW40Ppp/BH5vP8w85g8oc2KJI3otPxtObqO6a5u6xtXI27xYERZUWkFL7Se1vHgjp7iPLlB94WyH0Va3rYcDxh7vVRtRZVltBVWVmRaIRdaT2vKwYWB42kHeW1oO6q0jLa+N967jzC6ra9Hl+V2fzusrIi0QrOznpbX9A1EZVPKtNx3fX+0f49qZFvKstzuv5unFfaA1ta0auVrmq+QbanTevsHQ7uq2/qauqyItkKnEf5U2da0Dsklui+RXSkz+cefW+5Xt2VaVnhbIcMIL6vNaWUv46OlNmVSVou6raf/ZVpWWFvBWUSU1ea0kExpecpStvX0O//7t6W2Aj8+sizS0odpWYq2vvXO679909Ljln98UWWRlj7My4pqyyjr92/+10pbvvFFl0Va+milrIi2fvhPo6w/vPGfv8Ruyzs+RVmmaZ2dXdbQ0NDQ2LBKTG0Q+dXVO869sqO6umxOmRBzGqdWN1WvOn+h+sI5ITLP5oviMt3XzQZaKyusrS/4yvrjn96K3ZbnfqvKMk1r0udWzJ20cu6Ka8vFmWuZTfUis7liuxCivOG0KJ8qxGwhJmSJzJ1CvFI2W+TM1X3dbKDVskLaCpZ16VLstoRZWa2kJcSXJ4g5nrSaPQ9KFWXbt9eXr9pRH0yrtFKIX2fvLCYtG2i9rEBboWVZaMu0rFhpzbpWJiZcK19ufKvizIoV+eWN9TvO+dLauXNlvmiunDP1PGnZQGgUb0eX5WsrvKzYbZmWFSutokkvn395ZXZTsRDLvE+IjeL02cCjVuUyUV1RUdFUQFrXv1hledp6IqKsmG2ZlhUrLbHq2rVJp0Xzyp81NVY0VVYuL28U4kwgreILBZVCiFUrVlZWLtd96a53Mctqabn/idcjy4rVlmlZpmnlFwiRb/w2moJmY4tFyZwCUZqVlVVfVOrd2rpIiJxsIQqWlhr7qHOMi3Rfuutd7LJa/qooK0Zbz5r9LN7X0oeFsv6mKqvVtq5c/ihpaa/dZbXS1pXLl03bIi1ttL8s07aMskzbIi1txFGWSVvesszaIi1txFOWsi1/WSZtkZY24ipL0VawLHVbpKWN+MqKaiu0LGVbpKWNOMuKaCu8LFVbpKWNeMsKayuyLEVbpKWNuMsKaSu6rOi2SEsb8ZcVaEtVVlRbpKWNBJTla0tdVmRbpKWNRJTlacusrIi2SEsbCSnr0qW3rvzO3LOkpaPElBXDl0hLQ6qy3khwWaFtkZY23pWyQtoiLW28O2UF2yItbfwG6ACkhQ4ihPhImF+eXLht27ZtL730vg7wq61bt279xXuhA93PBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGCZEOL/q2XBkWi9EiUAAAAASUVORK5CYII=\" data-filename=\"footer.png\" style=\"font-size: 20px; width: 308px; height: 103.349px; float: none;\">otextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextãotextão</li></ol></h2></h5><ul><li><span style=\"background-color: rgb(255, 255, 0);\">Item 2</span></li></ul><p><br></p></blockquote>', '2019-07-05 00:00:00', NULL, 3, 5, NULL, '2019-06-27 14:36:53', 0, NULL, NULL, NULL, NULL),
(17, 'Comunicado jdahfkjhald', 2, '<p>lh\\dgoiho\\dahg</p><ul><li>sgdhçljahlkhad</li></ul><p><br></p><table class=\"table table-bordered\"><tbody><tr><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td></tr></tbody></table><p><br></p><p>l\\daglj\\hda</p>', '2019-07-05 00:00:00', NULL, 3, 5, NULL, '2019-06-27 15:06:01', 0, 'files/KuvPiEVEpq3KPloTMV1uIuB1x8ltu2HqnyYPw9I7.png', '/storage/files/KuvPiEVEpq3KPloTMV1uIuB1x8ltu2HqnyYPw9I7.png', 'footer.png', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(5, 'Comercial', '2019-06-06 17:57:44', '2019-06-06 17:57:44'),
(6, 'Fabrica', '2019-06-06 17:57:51', '2019-06-06 17:57:51'),
(7, 'Diretoria', '2019-06-06 17:58:18', '2019-06-06 17:58:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2014_10_12_000000_create_users_table', 1),
(16, '2014_10_12_100000_create_password_resets_table', 1),
(17, '2019_06_05_153638_create_groups_table', 1),
(18, '2019_06_05_153742_create_bulletins_table', 1),
(19, '2019_06_05_153838_create_users_groups_table', 1),
(20, '2019_06_06_160334_add_field_bulletins_table', 2),
(21, '2019_06_14_150413_alter_fields_bulletin_table', 3),
(22, '2019_06_14_155848_drop_file_path_column_bulletin_table', 4),
(24, '2019_06_14_164112_add_field_bulletin_table', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval` tinyint(1) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `approval`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mailtrap', '6ffc2e9ecd-aedeea@inbox.mailtrap.io', 1, NULL, '$2y$10$58BEMGL6rcrh2c5l3/22muKkKGsfTMgnl9FrkYjTOAiVbTnwNRo9e', 'NqVWbDIdCwNzzHgKAdOqCoJpsb02FOVyzVpvPBKEps4rIppDsrg9vScBGS6C', '2019-06-05 19:15:58', '2019-06-25 18:20:44'),
(2, 'Anderson', 'andersoncassoli@gmail.com', NULL, NULL, '$2y$10$WkGD8AYKFvm3i1CSXA1g9OCItnYObT.Awdr9xtvWaXXvRjnUz8Hlm', NULL, '2019-06-26 19:30:09', '2019-06-26 19:30:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_groups`
--

CREATE TABLE `users_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(5, 1, 5, NULL, NULL),
(7, 1, 6, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulletins`
--
ALTER TABLE `bulletins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bulletins_group_id_foreign` (`group_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_groups_user_id_foreign` (`user_id`),
  ADD KEY `users_groups_group_id_foreign` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulletins`
--
ALTER TABLE `bulletins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `bulletins`
--
ALTER TABLE `bulletins`
  ADD CONSTRAINT `bulletins_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Limitadores para a tabela `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `users_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
