@extends('layouts.app')

@section('content')
    <div class="modal fade" id="modalCreateGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Grupos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'group.store', 'class' => 'form']) !!}
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fas fa-users prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="defaultForm-email">Nome do Grupo</label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                </div>

                <div class="md-form mb-4">
                    <i class="fas fa-user prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Contatos</label>
                    {!! Form::select('contacts[]', $contacts, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple', 'id' => 'contacts']) !!}
                </div>

            </div>
                <div class="modal-footer d-flex justify-content-center">
                    {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Grupos</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('group.index') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Grupos</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>
    <div class='buttons' style="width:100%; overflow: auto; white-space: nowrap; margin:0px auto; padding-left:10px">
        <div style='float: left;'>
            <a href="{{ route('group.create') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" float="right"><i class="fa fa-plus"></i>&nbsp;Novo</a>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>@sortablelink('id', '#')</td>            
                                <td>@sortablelink('name', 'Nome')</td>
                                <td>Usuários</td>
                                <td></td>
                            </tr>
                            @foreach($groups as $group)
                            <tr>
                                <td>{{$group->id}}</td>
                                <td>{{$group->name}}</td>
                                <td>
                                    @foreach ($group->users as $user)
                                        {{ $user->name }}, 
                                    @endforeach
                                </td>
                                <td>
                                <a href="{{ route('group.edit', $group->id) }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-edit"></i>&nbsp;Editar</a>
                                </td>
                            </tr>
                            @endforeach
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection