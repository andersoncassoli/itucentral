@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Comunicados</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Comunicados Publicados</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Comunicados</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <!--
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                            -->
                        </div>
                    </div>
                    <div class="ibox-content">
                        <!--@can('view_bulletin')-->
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Data</th>
                                    <th>Assunto</th>
                                    <th>Prioridade</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($bulletins as $bullet)
                                        @if($bullet->pivot->read_at === null)
                                            <tr style="font-weight: bold;">
                                        @else
                                            <tr>
                                        @endif
                                            <td>{{$bullet->id}}</td>
                                            <td>{{ date('d-m-Y', strtotime($bullet->date_post)) }}</td>
                                            <td>{{$bullet->subject}}</td>
                                            @switch($bullet->priority)
                                                @case(1)
                                                    <td>Alta</td>
                                                    @break
                                                @case(2)
                                                    <td>Média</td>
                                                    @break
                                                @case(3)
                                                    <td>Baixa</td>
                                                    @break
                                            @endswitch                                        
                                            <td>
                                                <a href="{{ route('bulletin.show', $bullet->id) }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-eye"></i>&nbsp;Exibir</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        {{ $bulletins->links() }}
                        <!--@endcan-->
                    </div>
                </div>
            </div>
		</div>
 @endsection
