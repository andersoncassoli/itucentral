@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Novo Grupo</h2>
        <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('group.index') }}">Grupos</a>
        </li>
        <li class="breadcrumb-item active">
            <strong>Novo Grupo</strong>
        </li>
        </ol>
    </div>
    </div>
        @if( isset($errors) && count($errors) > 0 )
            <div class="alert alert-danger">
                @foreach( $errors->all() as $error )
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif

        <div class="wrapper wrapper-content animated">
               <div class="row">
                   <div class="col-md-12">
                   <div class="ibox ">
                        <div class="ibox-content">
                               {!! Form::open(['route' => 'group.store', 'class' => 'form', 'id' => 'form', 'enctype' => 'multipart/form-data']) !!}
                                   <div class="form-group row"><label class="col-lg-2 col-form-label">Nome do grupo</label>

                                       <div class="col-lg-10">{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required']) !!}
                                       </div>
                                   </div>
                                
                                    <div class="form-group row"><label class="col-lg-2 col-form-label">Usuários</label>
                                        <div class="col-lg-10">
                                            {!! Form::select('users[]', $users, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'users_id', 'required']) !!}
                                        </div>
                                    </div>
                                   <div class="form-group row">
                                       <div class="col-lg-offset-2 col-lg-10">
                                           <button class="btn btn-secondary" type="submit">Salvar</button>
                                       </div>
                                   </div>
                               </form>
                           </div>


                   </div>
               </div>
   		</div>
 @endsection
