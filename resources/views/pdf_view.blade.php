<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <head>
        <style>
            /** Define the margins of your page **/
            @page {
                margin: 40px 25px;
            }

            header {
                position: fixed;
                top: -10px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                background-image: url("/img/1x/header_print.png");
                /** background-color: #999999; **/
                color: black;
                font-size: 20px;
                text-align: center;
                line-height: 30px;
            }

            footer {
                position: fixed; 
                bottom: 20px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /** background-image: url("/img/1x/footer_print.png"); **/
                background-color: #c3c3c3;
                color: white;
                text-align: center;
                line-height: 30px;
            }

            .pagenum:before {
                  content: counter(page);
            }
        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
          {{ $heading }}
        </header>

        <footer>
          <span class="pagenum" style="color: #000000; text-align: center;"> </span>
        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
          {!!html_entity_decode($content)!!}
        </main>
    </body>
</html>