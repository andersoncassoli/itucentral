@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
    <h2>Editar Perfil de Acesso: <b> {{ $roles->id }} </b></h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('role.index') }}">Perfis</a>
        </li>
      <li class="breadcrumb-item active">
        <strong>Editar Perfil</strong>
      </li>
    </ol>
  </div>
</div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox ">
        <div class="ibox-content">
          {!! Form::model($roles, ['route' => ['role.update', $roles->id], 'class' => 'input-form', 'id' => 'form', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
          {!! Form::hidden('id', $roles->id, ['class' => 'form-control', 'placeholder' => 'Email']) !!}

            <div class="form-group row"><label class="col-lg-2 col-form-label">Nome</label>
              <div class="col-lg-10">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
              </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label">Usuários</label>
              <div class="col-lg-10">
                {!! Form::select('users[]', $users, $selected_users, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'users_id']) !!}
              </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label">Permissões</label>
              <div class="col-lg-10">
                {!! Form::select('permissions[]', $permissions, $selected_permissions, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'permission_id']) !!}
              </div>
            </div>
          <div class="form-row">
              <div class="form-group2 col-md-11">  
                  {!! Form::submit('Salvar', ['class' => 'btn btn-success', 'value' => 'salvar']) !!}
              </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
