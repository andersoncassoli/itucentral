@extends('layouts.test')

@section('content')
<div class="ibox ">
    <div class="ibox-content">
        {!! Form::open(['route' => 'test_store', 'class' => 'form', 'id' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

        <div class="form-group row">
            <label class="col-lg-2 col-form-label">Nome</label>
            <div class="col-lg-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required']) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="document">Documents</label>
            <div class="needsclick dropzone" id="document-dropzone">

            </div>
        </div>

        <!-- {!! Form::file('file', null, ['class' => 'form-control', 'placeholder' => 'Arquivo']) !!} -->
        <div class="form-row">
            <div class="form-group2 col-md-11">
                {!! Form::button('<i class="fa fa-save"></i>&nbsp;&nbsp;Salvar e Exibir', ['type' => 'submit', 'class' => 'btn btn-success btn-lg'] )  !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection