@extends('layouts.app')

@section('content')
<div class="modal fade" id="modalComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Rejeitar Comunicado</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {!! Form::open(['route' => 'bulletinno', 'class' => 'form', 'method' => 'PUT']) !!}
        {!! Form::hidden('id', $bulletin->id, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
        <div class="modal-body mx-10">
            <div class="md-form mb-10">
                <i class="fa fa-info-circle"></i>
                <label data-error="wrong" data-success="right" for="defaultForm-email">Informe abaixo o motivo da rejeição:</label>
                {!! Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => '']) !!}
            </div>
        </div>
            <div class="modal-footer d-flex justify-content-center">
                {!! Form::submit('Enviar', ['class' => 'btn btn-success btn-lg']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
    <h2>Visualizar Comunicado: <b> {{  $bulletin->id }} - {{  $bulletin->subject }}</b></h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        @can('edit_bulletin')
            <li class="breadcrumb-item">
                <a href="{{ url('requestbulletin') }}">Meus Comunicados</a>
            </li>
        @elsecan('approval_bulletin')
            <li class="breadcrumb-item">
                <a href="{{ url('requestbulletin') }}">Meus Comunicados</a>
            </li>
        @elsecan('view_bulletin')
            <li class="breadcrumb-item">
                <a href="{{ route('bulletin.index') }}">Comunicados</a>
            </li>
        @endcan
      <li class="breadcrumb-item active">
        <strong>Exibir</strong>
      </li>
    </ol>
  </div>
</div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
<br>
<div class='buttons' style="width:50%; overflow: auto; white-space: nowrap; margin:0px auto;">
    @can('edit_bulletin')
        <div style='float: left;'>
            @if ($bulletin->status < 4)
                <a href="{{ route('bulletin.edit', $bulletin->id) }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" float="right"><i class="fa fa-edit"></i>&nbsp;&nbsp;Editar</a>
            @endif
        </div>
        <div style='float: right;'>
            <a href="{{ url('/printpdf/') }}/{{$bulletin->id}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true" target="_blank" float="right"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimir</a>
        </div>
    @elsecannot('edit_bulletin')
        @if ($bulletin->status === 5)
            <div style='float: right;'>
                <a href="{{ url('/printpdf/') }}/{{$bulletin->id}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true" target="_blank" float="right"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimir</a>
            </div>
        @endif
    @endcan
</div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-content" style="width:840px; height: auto; margin: 0 auto; padding: 20px; position: relative;">
                    <div style="position:relative; width:800px; height:200px; background-image: url({{ asset('img/1x/header_scr_new_001.png') }}); align:center">
                        <table style="width:100%; height:40px;">
                            <tr>
                                <td style="width:50%; align:center; vertical-align: middle; text-align: center; font-size:18px; color:white;">Data: {{ date('d-m-Y', strtotime($bulletin->date_post)) }}</td>
                                <td style="width:50%; align:center; vertical-align: middle; text-align: center; font-size:18px; color:white;">N&ordm; {{  $bulletin->id }}</td>
                            <tr>
                        </table>
                        <div style="position:absolute; bottom:0px; left:20px; width:760px; vertical-align: middle; text-align: center; align:center; font-size:60px;">
                            <table style="width:100%; height:40px;">
                                <tr>
                                    <td style="width:100%; align:center; vertical-align: middle; text-align: center; font-size:24px;">{{  $bulletin->subject }}</td>
                                <tr>
                            </table>
                        </div>
                    </div>
                    <div style="width:800px;">
                        <br>
                        {!!html_entity_decode($bulletin->message)!!}
                    </div>
                    <div style="width:800px; height:40px; background-image: url({{ asset('img/1x/footer_scr_new_001.png') }})">
                        <table style="width:100%; height:60px;">
                            <tr>
                                <td style="width:100%; align:center; vertical-align: middle; text-align: center; font-size:18px; color:white;">{{ $bulletin->area }}</td>
                            <tr>
                        </table>
                    </div>
                    @if (count($files) > 0)
                        <div class="ibox-content">
                            <label class="col-lg-2 col-form-label">Arquivos</label>
                            <table>
                                @foreach ($files as $file)
                                <tr>
                                    <td>
                                        <a href="{{$file->file_url}}" target="_blank">{{$file->file_original_name}}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                    <!--
                    @if ( isset($bulletin->file_original_name))
                    <div class="ibox-content">
                        <label class="col-lg-2 col-form-label">Arquivos</label>
                            <a href="{{ $url }}" target="_blank">{{$bulletin->file_original_name}}</a>
                    </div>
                    @endif
                    -->
                    
                </div>
            </div>
            <div style='width:840px; height: auto; margin: 0 auto; padding: 0px; position: relative; text-align: center'>
                
                
                {!! Form::model($bulletin, ['route' => ['bulletin.update', $bulletin->id], 'class' => 'form', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    {!! Form::hidden('id', $bulletin->id, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                @switch($bulletin->status)
                    @case(1)
                        @can ('edit_bulletin')
                            <button type="submit" id="btn-one" class="btn btn-success btn-lg" name="approval" formaction="/sendapproval" style="text-align: center;"><i class="fa fa-thumbs-up" style="color:white;"></i>&nbsp;&nbsp;Enviar para Aprovação&nbsp;&nbsp;<i class="fa fa-thumbs-down" style="color:white;"></i></button>
                        @endcan
                        @break
                    @case(2)
                        @can ('approve_bulletin')
                            <button class="btn btn-lg btn-success btn-lg" id="btn-one" type="submit" formaction="/bulletinyes"><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;Aprovar</button>&nbsp;&nbsp;&nbsp;
                            <a href="" class="btn btn-lg btn-danger" id="btn-two" data-toggle="modal" data-target="#modalComment"><i class="fa fa-thumbs-down"></i>&nbsp;&nbsp;Rejeitar</a>
                        @endcan
                        @break
                    @case(3)
                        @can('edit_bulletin')
                            <button type="submit" id="btn-one" class="btn btn-success btn-lg" name="approval" formaction="/sendapproval"><i class="fa fa-thumbs-up" style="color:white;"></i>&nbsp;&nbsp;Enviar para Aprovação&nbsp;&nbsp;<i class="fa fa-thumbs-down" style="color:white;"></i></button>
                        @endcan
                        @break
                    @case(4)
                        @can('edit_bulletin')
                            <button id="btn-one" class="btn btn-lg btn-success" type="submit" formaction="/sendpublish"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Publicar</button>
                        @endcan
                        @break
                @endswitch

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <br>
@endsection
