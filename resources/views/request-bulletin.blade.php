@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Meus Comunicados</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Meus Comunicados</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>
    @can ('create_bulletin')
        <div class='buttons' style="width:100%; overflow: auto; white-space: nowrap; margin:0px auto; padding-left:10px">
            <div style='float: left;'>
                <a href="{{ route('bulletin.create') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" float="right"><i class="fa fa-plus"></i>&nbsp;Novo</a>
            </div>
        </div>
    @endcan
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <!--
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                        -->
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 5%;">@sortablelink('id', '#')</th>
                            <th style="width: 10%;">@sortablelink('date_post', 'Data')</th>
                            <th style="width: 25%;">@sortablelink('subject', 'Assunto')</th>
                            <th style="width: 10%;">@sortablelink('priority', 'Prioridade')</th>
                            <th style="width: 10%;">@sortablelink('status', 'Status')</th>
                            <th style="width: 20%;">@sortablelink('created_at', 'Data Solicitação')</th>
                            <th style="width: 20%;"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @can('edit_bulletin')
                                @foreach($bulletins as $bullet)
                                    @switch($bullet->status)
                                        @case(0)
                                            <tr >
                                            @break
                                        @case(1)
                                            <tr style="background: #FFFCE3;">
                                        @break
                                        @case(2)
                                            <tr style="background: #FFEBE0;">
                                        @break
                                        @case(3)
                                            <tr style="background: #FFACAB; color:white;">
                                        @break
                                        @case(4)
                                            <tr style="background: #C8EBE1;">
                                        @break
                                        @case(5)
                                            <tr style="background: #CDE2F7;">
                                        @break
                                        @case(6)
                                            <tr style="background: #DADFE6;">
                                        @break
                                    @endswitch
                                    
                                        <td style="vertical-align: middle;">{{$bullet->id}}</td>
                                        <td style="vertical-align: middle;">{{ date('d-m-Y', strtotime($bullet->date_post)) }}</td>
                                        <td style="vertical-align: middle;">{{$bullet->subject}}</td>
                                        @switch($bullet->priority)
                                            @case(1)
                                                <td style="vertical-align: middle;">Alta</td>
                                                @break
                                            @case(2)
                                                <td style="vertical-align: middle;">Média</td>
                                                @break
                                            @case(3)
                                                <td style="vertical-align: middle;">Baixa</td>
                                                @break
                                        @endswitch                                        
                                        @switch($bullet->status)
                                            @case(0)
                                                <td style="vertical-align: middle;">Enviado</td>
                                                @break
                                            @case(1)
                                                <td style="vertical-align: middle;">Em edição</td>
                                                @break
                                            @case(2)
                                                <td style="vertical-align: middle;">Aguardando aprovação</td>
                                                @break
                                            @case(3)
                                                <td style="vertical-align: middle;">Rejeitado</td>
                                                @break
                                            @case(4)
                                                <td style="vertical-align: middle;">Aprovado</td>
                                                @break
                                            @case(5)
                                                <td style="vertical-align: middle;">Publicado</td>
                                                @break
                                            @case(6)
                                                <td style="vertical-align: middle;">Cancelado</td>
                                                @break
                                        @endswitch
                                        <td style="vertical-align: middle;">{{ date('d-m-Y h:i:s', strtotime($bullet->created_at)) }}</td>
                                        <td style="vertical-align: middle;">
                                            <a href="{{ route('view', $bullet->id) }}" class="btn btn-success btn-sm" role="button"><i class="fa fa-eye"></i>&nbsp;Exibir</a>                                            
                                            @if ( $bullet->status < 4 )
                                                <a href="{{ route('bulletin.edit', $bullet->id) }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-edit"></i>&nbsp;Editar</a>
                                            @else
                                                <a href="{{ route('bulletin.edit', $bullet->id) }}" class="btn btn-secondary btn-sm" role="button"><i class="fa fa-info-circle"></i>&nbsp;Detalhes</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @canany(['create_bulletin', 'approve_bulletin'])
                                    @foreach($my_bulletins as $bullet)
                                        <tr>
                                            <td>{{$bullet->id}}</td>
                                            <td>{{ date('d-m-Y', strtotime($bullet->date_post)) }}</td>
                                            <td>{{$bullet->subject}}</td>
                                            @switch($bullet->priority)
                                                @case(1)
                                                    <td>Alta</td>
                                                    @break
                                                @case(2)
                                                    <td>Média</td>
                                                    @break
                                                @case(3)
                                                    <td>Baixa</td>
                                                    @break
                                            @endswitch                                        
                                            @switch($bullet->status)
                                                @case(0)
                                                    <td>Enviado</td>
                                                    @break
                                                @case(1)
                                                    <td>Em edição</td>
                                                    @break
                                                @case(2)
                                                    <td>Aguardando aprovação</td>
                                                    @break
                                                @case(3)
                                                    <td>Rejeitado</td>
                                                    @break
                                                @case(4)
                                                    <td>Aprovado</td>
                                                    @break
                                                @case(5)
                                                    <td>Publicado</td>
                                                    @break
                                                @case(6)
                                                    <td>Cancelado</td>
                                                    @break
                                            @endswitch
                                            <td>{{ date('d-m-Y h:i:s', strtotime($bullet->created_at)) }}</td>
                                            <td>
                                                <a href="{{ route('bulletin.show', $bullet->id) }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-eye"></i>&nbsp;Exibir</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endcan
                            @endcan
                        </tbody>
                    </table>
                    @can('edit_bulletin')
                        {{ $bulletins->links() }}
                    @else
                        @canany(['create_bulletin', 'approve_bulletin'])
                            {{ $my_bulletins->links() }}
                        @endcan
                    @endcan
                </div>
            </div>
        </div>
    </div>
 @endsection
