@extends('layouts.home')

@section('content')
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-md-9">
				<h2>Início</h2>
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('home') }}">Home</a>
					</li>
				</ol>
			</div>
		</div>
		<section style="margin-top: 50px;">
			<div class="container">
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<img src="img/banner-cardapio.png" width="98.5%">
						</div>
					</div>
					<div class="row" style="margin-top: 35px;">
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-4">
									<a href="{{ route('bulletin.index') }}"><img src="img/bt-Comunicados.png" width="100%"></a>
								</div>
								<div class="col-md-4">
									<img src="img/bt-Compras.png" width="100%" style="opacity: 0.4;">
								</div>
								<div class="col-md-4">
									<img src="img/bt-Contratos.png" width="100%" style="opacity: 0.4;">
								</div>
							</div>
							<div class="row">
								@php
									if (isset($_GET['open_folder'])) $linkchoice=$_GET['open_folder'];
									else $linkchoice='';

									switch($linkchoice){
										case '1' :
											$path = "R:\\Revendas\\Publicações\\Revendas\\R_Recursos Humanos";
											exec("explorer.exe $path");
											route('home');
											break;

										case '2' :
											$path = "R:\\Publicações\\Revendas\\R_Documentos";
											exec("explorer.exe $path");
											break;

										case '3' :
											$path = "R:\\Publicações\\Revendas\\R_Documentos\\Logística\\Levantamento Logístico\\Ferramenta de Auditoria Oficial.xls";
											exec("explorer.exe $path");
										break;
									}
								@endphp
								<!--
									<div class="col-md-4">
										<a href="?open_folder=1"><img src="img/bt-RecHum.png" width="100%"></a>
									</div>
									<div class="col-md-4">
										<a href="?open_folder=2" target="_blank"><img src="img/bt-GestDoc.png" width="100%"></a>
									</div>
									<div class="col-md-4">
										<a href="?open_folder=3" target="_blank"><img src="img/bt-LevLog.png" width="100%"></a>
									</div>
								-->
								<div class="col-md-4">
									<img src="img/bt-RecHum.png" width="100%" style="opacity: 0.4;">
								</div>
								<div class="col-md-4">
									<img src="img/bt-GestDoc.png" width="100%" style="opacity: 0.4;">
								</div>
								<div class="col-md-4">
									<img src="img/bt-LevLog.png" width="100%" style="opacity: 0.4;">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<img src="img/bt-rdv.png" width="100%" style="opacity: 0.4;">
								</div>
								<div class="col-md-4">
									<img src="img/bt-Aniversarios.png" width="100%" style="opacity: 0.4;">
								</div>
								<div class="col-md-4">
									<!-- <a href="{{ url('/contact/*') }}"><img src="img/bt-Contatos.png" width="100%"></a> -->
									<img src="img/bt-Contatos.png" width="100%" style="opacity: 0.4;">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<a href="http://painelrh.ddns.com.br:3000/painelrh/" target="_blank"><img src="img/bt-rh01.png" width="100%"></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<a href="https://www.wtscorporate.com.br/wtscorporate/login/" target="_blank"><img src="img/bt-viagens.png" width="100%"></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<a href="https://www.autotrac-online.com.br/LogCenter/Web/Login/Index.aspx" target="_blank" rel="noopener noreferrer"><img src="img/bt-logCenter.png" width="100%"></a>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>


<!-- Portfolio Item Heading -->
				<div class="wrapper wrapper-content animated fadeInRight">		
<!-- Portfolio Item Row -->
					
				
			
				<br>&nbsp;	
@endsection