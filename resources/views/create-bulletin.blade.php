@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Novo Comunicado</h2>
        <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('bulletin.index') }}">Comunicados</a>
        </li>
        <li class="breadcrumb-item active">
            <strong>Novo Comunicado</strong>
        </li>
        </ol>
    </div>
    </div>
        @if( isset($errors) && count($errors) > 0 )
            <div class="alert alert-danger">
                @foreach( $errors->all() as $error )
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif

        <div class="wrapper wrapper-content animated">
               <div class="row">
                   <div class="col-md-12">
                   <div class="ibox ">
                        <div class="ibox-content">
                               {!! Form::open(['route' => 'bulletin.store', 'class' => 'form', 'id' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                                   <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Assunto</label>
                                        <div class="col-lg-10">
                                                {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Assunto', 'required']) !!}
                                        </div>
                                   </div>
                                   <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox ">
                                                <div class="ibox-content no-padding">
                                                    {!! Form::textarea('message', null, ['name' => 'message', 'class' => 'summernote', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Público Alvo</label>
                                        <div class="col-lg-4">
                                            {!! Form::select('groups[]', $groups, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'groups_id', 'required']) !!}
                                        </div>
                                        <label class="col-lg-1 col-form-label">Usuários</label>
                                        <div class="col-lg-5">
                                            {!! Form::select('users[]', $users, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'users_id']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Data da publicação</label>
                                        <div class="col-lg-4">
                                            {!! Form::text('date_post', null, ['class' => 'form-control', 'id' => 'datepicker', 'required']) !!}
                                        </div>
                                        <label class="col-lg-1 col-form-label">Prioridade</label>
                                        <div class="col-lg-5">
                                            {!! Form::select('priorities[]', array('1' => 'Alta', '2' => 'Média', '3' => 'Baixa'), null, ['class' => 'form-control select2', 'id' => 'groups']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Incluir Arquivo</label>
                                        <div class="col-lg-10">
                                            <div class="needsclick dropzone" id="document-dropzone" style="border: dashed 1px gray; background-color: white;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-success btn-lg" type="submit"><i class="fa fa-arrow-circle-up"></i>&nbsp;&nbsp;Enviar Solicitação</button>
                                            <!--<button class="btn btn-secondary btn-lg" type="submit">Salvar Sem Enviar</button>
                                            <button class="btn btn-success btn-lg " type="submit" formaction="/sendbulletin">Enviar Solicitação</button>-->
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                           </div>
                   </div>
               </div>
   		</div>
 @endsection
