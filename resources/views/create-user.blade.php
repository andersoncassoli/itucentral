@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Novo Usuário</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('user.index') }}">Usuários</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Novo</strong>
                </li>
            </ol>
        </div>
    </div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
  <div class="wrapper wrapper-content animated">
    <div class="row">
      <div class="col-md-12">
        <div class="ibox ">
          <div class="ibox-content">
            {!! Form::open(['route' => 'user.store', 'class' => 'form', 'id' => 'form']) !!}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Nome</label>
                    <div class="col-lg-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Email</label>
                    <div class="col-lg-6">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group2 col-md-11">  
                        {!! Form::submit('Incluir', ['class' => 'btn btn-success', 'value' => 'salvar']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
