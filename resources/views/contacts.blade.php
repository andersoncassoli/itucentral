@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contatos</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Contatos</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>
    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div align="center">  
            @php  
                $character = range('A', 'Z');
                array_unshift($character, "*");
            @endphp
            <ul class="pagination">
                @foreach($character as $alphabet)  
                    <li><a href="{{ url('/contact/') }}/{{$alphabet}}">{{ $alphabet }}</a></li> 
                @endforeach
            </ul>
        </div>  
        <div class="row">
            @foreach($users as $user)
                <div class="col-lg-4">
                    <div class="contact-box">
                        <!--<a class="row" href="#">-->
                        <div class="row">
                            <div class="col-4">
                                <div class="text-center">
                                    <img alt="image" class="rounded-circle m-t-xs img-fluid" src="{{ asset('img/user_icon_defaultnv.png') }}">
                                    <div class="m-t-xs font-bold"></div>
                                </div>
                            </div>
                            <div class="col-8">
                                <h3><strong>{{$user->name}}</strong></h3>
                                <!--<p><i class="fa fa-map-marker"></i> Riviera State 32/106</p>-->
                                <address style="word-wrap: break-word;">
                                    <strong>{{ $user->ou_name}}</strong><br>
                                    {{$user->city}}</a><br>
                                    <a href="mailto:{{$user->email}}">{{$user->email}}</a><br>
                                    <!--San Francisco, CA 94107<br>
                                    <abbr title="Phone">P:</abbr> (123) 456-7890-->
                                </address>
                            </div>
                        <!--</a>-->
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {!! $users->appends(\Request::except('page'))->render() !!}
 @endsection