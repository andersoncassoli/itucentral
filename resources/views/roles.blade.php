@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Perfis de Acesso</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('bulletin.index') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Perfis</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>

    <div class='buttons' style="width:100%; overflow: auto; white-space: nowrap; margin:0px auto; padding-left: 10px;">
        <div style='float: left;'>
            <a href="{{ route('role.create') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true"><i class="fa fa-plus"></i>&nbsp;Novo</a>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>ID</td>            
                                <td style="width:80px;">Nome</td>
                                <td>Usuários</td>
                                <td style="width:200px;">Permissões</td>
                                <td></td>
                            </tr>
                            @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td style="width:80px;">{{$role->name}}</td>
                                <td>
                                    @foreach ($role->users as $user)
                                        <span class="border border-warning">&nbsp;{{ $user->name }}&nbsp;</span>&nbsp;&nbsp;
                                    @endforeach
                                </td>
                                <td style="width:200px">
                                    @foreach ($role->permissions as $permission)
                                        <span class="border border-secondary">&nbsp;{{ $permission->label }}&nbsp;</span>&nbsp;&nbsp;
                                    @endforeach
                                </td>
                                <td>
                                <a href="{{ route('role.edit', $role->id) }}" class="btn btn-primary btn-sm" role="button"><i class="fa fa-edit"></i>&nbsp;Editar</a>
                                </td>
                            </tr>
                            @endforeach
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection