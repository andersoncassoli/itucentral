@extends('layouts.app')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Usuários</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Usuários</strong>
                </li>
            </ol>
        </div>
    </div>
    @if( isset($errors) && count($errors) > 0 )
        <div class="alert alert-danger">
            @foreach( $errors->all() as $error )
            <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <br>
    <div class='buttons' style="width:100%; overflow: auto; white-space: nowrap; margin:0px auto; padding-left:10px">
        <div style='float: left;'>
            <a href="{{ route('user.create') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" float="right"><i class="fa fa-plus"></i>&nbsp;Novo</a>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td style="width: 5%;">@sortablelink('id', '#')</td>            
                                    <td style="width: 10%;">@sortablelink('name', 'Nome')</td>
                                    <td style="width: 20%;">@sortablelink('email', 'E-mail')</td>
                                    <td style="width: 20%;">@sortablelink('description', 'Departamento')</td>
                                    <td style="width: 25%;">@sortablelink('created_at', 'Criação')</td>
                                    <td style="width: 10%;">Grupos</td>
                                    <td style="width: 5%;">Perfis de Acesso</td>
                                    <td style="width: 5%;"></td>
                                </tr>
                            </thead>
                            <tbody style="font-size:12px;">
                                @foreach($users as $user)
                                <tr>
                                    <td style="width: 5%;">{{$user->id}}</td>
                                    <td style="width: 10%;">{{$user->name}}</td>
                                    <td style="width: 20%;">{{$user->email}}</td>
                                    <td style="width: 20%;">{{$user->description}}</td>
                                    <td style="width: 20%;">{{$user->created_at}}</td>
                                    <td style="width: 15%;">
                                        <ul>
                                        @foreach ($user->groups as $group)
                                            <a href="{{ route('group.edit', $group->id) }}" class="btn btn-outline-secondary btn-xs" role="button">{{ $group->name }}</a>
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td style="width: 15%">
                                        <ul>
                                        @foreach ($user->roles as $role)
                                            <a href="{{ route('role.edit', $role->id) }}" class="btn btn-outline-secondary btn-xs" role="button">{{ $role->name }}</a>
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td style="width: 5%">
                                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-secondary btn-xs" role="button">Detalhes</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $users->appends(\Request::except('page'))->render() !!}
                        <!--{{ $users->links() }}-->
                    </div>
                </div>
            </div>
        </div>
 @endsection