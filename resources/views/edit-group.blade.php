@extends('layouts.app')

@section('content')
<div class="modal fade" id="modalComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
          <div class="modal-header text-center">
              <h4 class="modal-title w-100 font-weight-bold">Confirmar a exclusão do grupo: <b> {{ $groups->name }} </b> </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="text-center">
            <br>
            {!! Form::open(['route' => ['group.destroy', $groups->id], 'method' => 'DELETE']) !!}
              {!! Form::submit("Sim", ['class' => 'btn btn-danger']) !!}
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
            {!! Form:: close() !!} 
            <br>
          </div>
      </div>
  </div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
    <h2>Editar Grupo <b> {{ $groups->id }} </b></h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('group.index') }}">Grupos</a>
        </li>
      <li class="breadcrumb-item active">
        <strong>Editar Grupo</strong>
      </li>
    </ol>
  </div>
</div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox ">
        <div class="ibox-content">
          {!! Form::model($groups, ['route' => ['group.update', $groups->id], 'class' => 'input-form', 'id' => 'form', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
          {!! Form::hidden('id', $groups->id, ['class' => 'form-control', 'placeholder' => 'Email']) !!}

            <div class="form-group row"><label class="col-lg-2 col-form-label">Nome</label>
              <div class="col-lg-10">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required']) !!}
              </div>
            </div>
            <div class="form-group row"><label class="col-lg-2 col-form-label">Usuários</label>
              <div class="col-lg-10">
                {!! Form::select('users[]', $users, $selected_users, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'users_id']) !!}
              </div>
            </div>
          <div class="form-row">
              <div class="form-group2 col-md-12">  
                  <div style='float: left;'>
                    {!! Form::submit('Salvar', ['class' => 'btn btn-success', 'value' => 'salvar']) !!}
                  </div>
                  <div style='float: right;'>
                    <a href="" class="btn btn-danger" data-toggle="modal" data-target="#modalComment"><i class="fa fa-trash"></i>&nbsp;&nbsp;Excluir</a>
                  </div>
              </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
