@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
    <h2>Alterar Senha</h2>
  </div>
</div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
  <div class="wrapper wrapper-content animated">
    <div class="row">
      <div class="col-md-12">
        <div class="ibox ">
          <div class="ibox-content">
            {!! Form::open(['route' => 'postreset', 'class' => 'form', 'id' => 'form', 'method' => 'PUT']) !!}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Nome</label>
                    <div class="col-lg-6">
                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Nome', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Email</label>
                    <div class="col-lg-6">
                        {!! Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Nome', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Senha</label>
                    <div class="col-lg-6">
                        {!! Form::password('password', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Confirmar Senha</label>
                    <div class="col-lg-6">
                        {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group2 col-md-11">  
                        {!! Form::submit('Alterar', ['class' => 'btn btn-success', 'value' => 'salvar']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
