@extends('layouts.app')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
    <h2>Reenviar Comunicado: <b> {{ $bulletin->id }} </b></h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('requestbulletin') }}">Meus Comunicados</a>
        </li>
      <li class="breadcrumb-item active">
        <strong>Editar</strong>
      </li>
    </ol>
  </div>
</div>
@if( isset($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach( $errors->all() as $error )
        <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
@if (Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif
<div class="wrapper wrapper-content animated">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox ">
        <div class="ibox-content">
          {!! Form::model($bulletin, ['route' => ['bulletin.update', $bulletin->id], 'class' => 'input-form', 'id' => 'form', 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
          {!! Form::hidden('id', $bulletin->id, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            @if ($bulletin->owner->id != $auth->id and $bulletin->status == 0)
              <div class="form-group row">
                <label class="col-lg-2 col-form-label">Solicitado por: </label>
                <div class="col-lg-10">
                  {!! Form::text('owner', $bulletin->owner->name, ['class' => 'form-control', 'placeholder' => 'Nome', 'readonly']) !!}
                </div>
              </div>
            @endif
            <div class="form-group row">
              <label class="col-lg-2 col-form-label">Assunto</label>
              <div class="col-lg-6">
                {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required', 'readonly']) !!}
              </div>
              <label class="col-lg-2 col-form-label">Data da publicação</label>
              <div class="col-lg-2">
                {!! Form::text('date_post', $dt_string, ['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'Data', 'required']) !!}
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="ibox ">
                  <div class="ibox-content no-padding" style="border: solid 1px white; background-color: #E9ECEF;">
                     {!! Form::hidden('message', $bulletin->message, ['class' => 'form-control']) !!}
                    {!!html_entity_decode($bulletin->message)!!}
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-2 col-form-label">Prioridade</label>
              <div class="col-lg-4">
                {!! Form::select('priority[]', array('1' => 'Alta', '2' => 'Média', '3' => 'Baixa'), $bulletin->priority,  ['class' => 'form-control select2', 'placeholder' => 'Prioridade', 'id' => 'priority', 'readonly']) !!}
              </div>
              <label class="col-lg-2 col-form-label">Status</label>
              <div class="col-lg-4">
                {!! Form::select('status[]', array('0' => 'Enviado', '1' => 'Em edição', '2' => 'Aguardando aprovação', '3' => 'Rejeitado', '4' => 'Aprovado', '5' => 'Publicado', '6' => 'Cancelado'), $bulletin->status, ['class' => 'form-control select2', 'id' => 'status']) !!}
              </div>
              
            </div>
            <div class="form-group row">
              <label class="col-lg-2 col-form-label">Público Alvo (Grupos)</label>
              <div class="col-lg-4">
                {!! Form::select('groups[]', $groups, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'groups_id', 'required']) !!}
              </div>
              <label class="col-lg-2 col-form-label">Público Alvo (Usuários)</label>
              <div class="col-lg-4">
                {!! Form::select('users[]', $users, null, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'users_id']) !!}
              </div> 
            </div>
            <div class="form-group row">
              <label class="col-lg-1 col-form-label">Aprovadores</label>
              <div class="col-lg-1 popover-block-container">
                <button tabindex="0" type="button" class="btn btn-link popover-icon" data-popover-content="#unique-id" data-trigger="focus" data-toggle="popover" data-placement="right">
                  <i class="fa fa-info-circle" style="color:blue"></i>
                </button>
                <div id="unique-id" style="display:none;">
                  <div class="popover-body">
                    <table style="border: 1px solid #E4E4E4; width: 560px;">
                      @foreach ($selected_usersapp as $u_app)
                        <tr>
                          <td style="width: 20%; text-align:center; border: 1px solid #E4E4E4;">{{ $u_app->name }}</td>
                          <td style="width: 10%; text-align:center; border: 1px solid #E4E4E4;">&nbsp;@if ($u_app->pivot->acceptyn === 1) <i class="fa fa-thumbs-up" style="color:green;"></i>
                                                      @elseif ($u_app->pivot->acceptyn === 0) <i class="fa fa-thumbs-down" style="color:red;"></i> 
                                                      @else
                                                      @endif
                          </td>
                          <td style="width: 40%; text-align:left; border: 1px solid #E4E4E4;">{{ $u_app->pivot->comment }}</td>
                          <td style="width: 30%; text-align:left; border: 1px solid #E4E4E4;">{{ date('d-m-Y H:i', strtotime($u_app->pivot->updated_at)) }}</td>
                        <tr>
                      @endforeach
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                {!! Form::select('users_app[]', $users_app, $selected_usersapp, ['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'usersapp_id', 'required', 'readonly']) !!}
              </div>
              <label class="col-lg-2 col-form-label">Área Solicitante</label>
              <div class="col-lg-4">
                {!! Form::text('area', null, ['class' => 'form-control', 'placeholder' => 'Digite a área desejada...']) !!}
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-2 col-form-label">Comunicado original: <b> {{$bulletin->copied_from}} </b></label>
              <div class="col-lg-10">
                <a href="{{ route('bulletin.edit', $bulletin->copied_from) }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" float="right"><i class="fa fa-share"></i>&nbsp;Abrir Comunicado  {{$bulletin->copied_from}}</a>
              </div>
            </div>
            <br>
            <div class="form-row">
                <div class="form-group2 col-md-11">
                    {!! Form::button('<i class="fa fa-save"></i>&nbsp;&nbsp;Salvar e Exibir', ['type' => 'submit', 'class' => 'btn btn-success btn-lg'] )  !!}
                </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
