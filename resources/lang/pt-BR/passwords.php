<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha precisa ter pelo menos 8 caracteres e ser igual a confirmação.',
    'reset' => 'Sua senha está sendo enviada!',
    'sent' => 'Um e-mail foi enviado com o link para atualização da senha.',
    'token' => 'Esse token para cadastro de senha está incorreto.',
    'user' => "Seu e-mail não foi encontrado.",

];
