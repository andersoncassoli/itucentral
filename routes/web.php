<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/email', 'HomeController@email')->name('sendEmail');
Route::get('/contact/{character}', 'UserController@contact')->name('contact');
Route::get('/getad', 'UserController@getad')->name('getad');
Route::put('/sendtest', 'BulletinController@sendtest')->name('sendtest');
Route::put('/sendapproval', 'BulletinController@sendapproval')->name('sendapproval');
Route::put('/sendpublish', 'BulletinController@sendpublish')->name('sendpublish');
Route::put('/bulletinyes', 'BulletinController@bulletinyes')->name('bulletinyes');
Route::put('/bulletinno', 'BulletinController@bulletinno')->name('bulletinno');
Route::get('/printpdf/{id}', 'BulletinController@printpdf')->name('printpdf');
Route::get('/change_approvation/{id}', 'UserController@change_approvation')->name('change_approvation');
Route::put('/sendbulletin', 'BulletinController@sendbulletin')->name('sendbulletin');
Route::get('/removefile/{id}', 'BulletinController@removefile')->name('removefile');
Route::get('/requestbulletin', 'BulletinController@requestbulletin')->name('requestbulletin');
Route::get('/changepassword', 'UserController@changePassword')->name('changepassword');
Route::put('/postreset', 'UserController@postReset')->name('postreset');
Route::get('/acceptall/{id}', 'BulletinController@acceptall')->name('acceptall');
Route::get('/view/{id}', 'BulletinController@view')->name('view');
Route::get('/resend/{id}', 'BulletinController@resend')->name('resend');

Route::resource('/group', 'GroupController');
Route::resource('/bulletin', 'BulletinController');
Route::resource('/user', 'UserController');
Route::resource('/role', 'RoleController');
Route::resource('/file', 'FileController');

Route::post('/filepost', 'FileController@store')->name('filepost');

Route::get('/test/{id}', 'BulletinController@test')->name('test');
Route::post('/test_store', 'BulletinController@test_store')->name('test_store');

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
